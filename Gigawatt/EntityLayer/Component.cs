﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer
{
    public class Component
    {
        public int Id { get; set; }
        public int MainId { get; set; }
        public int ComponentId { get; set; }
        public int Quantity { get; set; } = 1;
    }
}

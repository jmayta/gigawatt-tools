﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityLayer.Universal;

namespace EntityLayer
{
    public class Partner
    {
        public int? Id { get; set; }
        // Unique fiscal number a.k.a. RUC
        public string TaxIdNumber { get; set; }
        // Unique fiscal name a.k.a. Rz Social
        public string BusinessName{ get; set; }
        public string? TradeName { get; set; }
        public string? Alias { get; set; }
        public string? FiscalAddress { get; set; }
        public string? Location { get; set; }
        public string? Ubigeo { get; set; }
        public int? CountryId { get; set; } = null;
        public bool IsActive { get; set; }
        public bool IsVendor { get; set; }
        public bool IsCustomer { get; set; }
        public bool IsBank { get; set; } = false;
        public string? Email { get; set; }
        public DateTime? DeactivationDate { get; set; } = null;
        public DateTime? CreatedAt { get; set; } = null;
        public Country? Country { get; set; } = null;
    }
}

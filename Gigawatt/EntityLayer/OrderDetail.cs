﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityLayer.Universal;

namespace EntityLayer
{
    public class OrderDetail
    {
        public long Id { get; set; }
        public string? ExternalCode { get; set; } = null;
        public double Demand { get; set; } = 1.00D;
        public decimal UnitPrice { get; set; } = 0.00M;
        public decimal? ValueOrder { get; set; } = 0.00M;
        public int OrderId { get; set; }
        public int ThingId { get; set; }
        public int UomId { get; set; }
        // Behavior
        public Order OrderObject { get; set; }
        public Thing ThingObject { get; set; }
        public UnitOfMeasurement UomObject { get; set; }
    }
}

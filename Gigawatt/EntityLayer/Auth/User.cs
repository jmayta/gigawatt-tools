﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer.Auth
{
    public class User
    {
        public int? Id { get; set; } = null;
        public string Username { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public DateTime? LastLogin { get; set; }
        public bool? IsAuthenticated { get; set; }
        public DateTime CreatedAt { get; set;  }
        public List<Group> Groups { get; set; }
        public List<Permission> Permissions { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer.Auth
{
    public class Permission
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CodeName { get; set; }
        public List<User> Users { get; set; }
        public List<Group> Groups { get; set; }
    }
}

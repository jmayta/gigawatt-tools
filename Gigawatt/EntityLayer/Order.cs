﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityLayer.Auth;
using EntityLayer.Universal;


namespace EntityLayer
{
    public class Order
    {
        public int? Id { get; set; } = null;
        public Guid Uuid { get; set; }
        public string? Code { get; set; }
        public string? ExternalDocument { get; set; } = null;
        public int? ExpenseTypeId { get; set; } = null;
        public int AuthorId { get; set; }
        public decimal? AmountUntaxed { get; set; } = 0.00M;
        public decimal? AmountTax { get; set; } = 0.00M;
        public decimal AmountTotal { get; set; } = 0.00M;
        public string? Observation { get; set; } = null;
        public int? RequisitionId { get; set; } = null;
        public string Status { get; set; } = Behavable.Statusable.Pending;
        public string? TermsConditions { get; set; } = null;
        public int CurrencyId { get; set; }
        public int ProjectId { get; set; }
        public int VendorId { get; set; }
        public DateTime CreatedAt { get; set; }
        // Behavior
        public ExpenseType? ExpenseTypeObject { get; set; }
        public User AuthorObject { get; set; }
        public Requisition? RequisitionObject { get; set; }
        public Currency CurrencyObject { get; set; }
        public Project ProjectObject { get; set; }
        public Partner VendorObject { get; set; }
        public List<OrderDetail>? OrderDetails { get; set; }
    }
}
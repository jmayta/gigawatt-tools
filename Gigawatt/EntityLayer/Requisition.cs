﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer
{
    public class Requisition : Behavable
    {
        public int Id { get; set; }
        public Guid Uuid { get; set; }
        public string Priority { get; set; } = Prioritable.Low;
        public string Status { get; set; } = Statusable.Pending;
        public string Observations { get; set; }
        public DateTime CreatedAt { get; set; }
        public int ApplicantId { get; set; }
        public int ProjectId { get; set; }
        public Auth.User Applicant { get; set; }
        public Project Project { get; set; }
        List<RequisitionDetail> Details { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer
{
    public class RequisitionDetail
    {
        public long Id { get; set; }
        public double Demand { get; set; } = 1D;
        public double BaseDemand { get; set; } = 0D;
        public bool IsPermanent { get; set; } = true;
        public DateTime RequestedFrom { get; set; }
        public DateTime RequestedUntil { get; set; }
        public DateTime CreatedAt { get; set; }
        public int RequisitionId { get; set; }
        public int ThingId { get; set; }
        public int UomId { get; set; }
        public Requisition Requisition { get; set; }
        public Thing Thing { get; set; }
        public Universal.UnitOfMeasurement Uom { get; set; }
    }
}

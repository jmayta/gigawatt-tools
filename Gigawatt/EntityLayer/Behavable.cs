﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer
{
    public abstract class Behavable
    {
        /* BEHAV-ABLE Class
         * En Django existen patrones avanzados para manejar la "Complejidad de Modelo",
         * uno de los más aceptados es el "Compositional Model Behaviors". 
         * Model Behaviors adopta la idea de composición y encapsulación a través del uso
         * de mixins. Los modelos heredan la lógica de los modelos abstractos. Para 
         * obtener más información, consulte los siguientes recursos:
         *  - https://docs.djangoproject.com/en/4.1/topics/db/models/#model-inheritance
         *  - https://blog.kevinastone.com/django-model-behaviors
         */
        public static class Prioritable
        {
            public static readonly string Urgent = "urgent";
            public static readonly string High = "high";
            public static readonly string Medium = "medium";
            public static readonly string Low = "low";
        }

        public static class Statusable
        {
            public static readonly string Pending = "pending";
            public static readonly string InProgress = "in progress";
            public static readonly string Done = "done";
            public static readonly string Canceled = "canceled";
        }

        public static class Kindable
        {
            public static readonly string Storable = "storable";
            public static readonly string Consumable = "consumable";
            public static readonly string Service = "service";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer.Universal
{
    public class ExpenseType
    {
        public int? Id { get; set; } = null;
        public string? AlphaCode { get; set; } = null;
        public string Description { get; set; }
        public int? ParentId { get; set; } = null;
    }
}

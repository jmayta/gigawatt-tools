﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer.Universal
{
    public class Currency
    {
        public int? Id { get; set; } = null;
        public string Name { get; set; }
        public string? PluralName { get; set; } = null;
        // ISO 4217
        public string Code { get; set; }
        public string? Symbol { get; set; } = null;
    }
}

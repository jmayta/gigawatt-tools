﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer.Universal
{
    public class UnitOfMeasurement
    {
        public int? Id { get; set; } = null;
        public string Name { get; set; }
        public string? NamePlural { get; set; } = null;
        public string Abbreviation { get; set; }
        public double? NumericalValue { get; set; } = 1.00D;
        public int? BaseUnitId { get; set; } = null;
        public UnitOfMeasurement? BaseUnit { get; set; }
        public List<UnitOfMeasurement>? ChildrenUnits { get; set; }
    }
}

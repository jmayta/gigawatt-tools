﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer.Universal
{
    public class Country
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Alpha2 { get; set; }
    }
}

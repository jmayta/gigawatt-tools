﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityLayer.Universal;

namespace EntityLayer
{
    public class Thing: Behavable
    {
        public int? Id { get; set; }
        public string Code { get; set; }
        public string? Unspsc { get; set; } = null;
        public string Description { get; set; }
        public int? DefaultUom { get; set; } = null;
        public string Kind { get; set; } = Behavable.Kindable.Consumable;
        public string? PicturePath { get; set; } = null;
        public string? BlueprintPath { get; set; } = null;
        public string? ManualPath { get; set; } = null;
        public double? Weight { get; set; } = 0D;
        public UnitOfMeasurement? Uom { get; set; } = null;
    }
}

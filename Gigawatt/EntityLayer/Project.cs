﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer
{
    public class Project
    {
        public int? Id { get; set; } = null;
        public string? Code { get; set; }
        public string Name { get; set; }
        public bool IsOwned { get; set; } = false;
        public DateTime? AwardDate { get; set; }
        public decimal? Budget { get; set; } = 0.00M;
        public int CustomerId { get; set; }
        public int? ParentId { get; set; } = null;
        public bool IsActive { get; set; } = true;
        public DateTime? CreatedAt { get; set; } = null;
        // Behavior
        public Partner? PartnerObject { get; set; }         // Customer
        public Project? ProjectObject { get; set; }         // Parent Project
        public List<Project>? ChildrenProjects { get; set;} // Children Projects
    }
}

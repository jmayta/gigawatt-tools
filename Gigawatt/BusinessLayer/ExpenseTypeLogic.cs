﻿using DataLayer;
using EntityLayer.Universal;

namespace BusinessLayer
{
    public class ExpenseTypeLogic
    {
        private readonly ExpenseTypeEngine _expenseTypeEngine = new ExpenseTypeEngine();

        public List<ExpenseType> GetExpenseTypes() { 
            return _expenseTypeEngine.GetExpenseTypes();
        }
    }
}

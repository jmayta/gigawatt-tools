﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataLayer;
using EntityLayer.Universal;

namespace BusinessLayer
{
    public class UnitOfMeasurementLogic
    {

        private readonly UnitOfMeasurementEngine uomEngine = new();

        public List<UnitOfMeasurement> GetUoms(bool withChild = true)
        {
            return uomEngine.GetUoms(withChild);
        }

        public int InsertUom(UnitOfMeasurement uomObject )
        {
            return uomEngine.InsertUom(uomObject);
        }

        public int UpdateUom(UnitOfMeasurement uomObject)
        {
            return uomEngine.UpdateUom(uomObject);
        }

        public int RemoveUom(UnitOfMeasurement uomObject)
        {
            return uomEngine.RemoveUom(uomObject);
        }
    }
}

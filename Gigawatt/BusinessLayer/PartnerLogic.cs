﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataLayer;
using EntityLayer;

namespace BusinessLayer
{
    public class PartnerLogic
    {
        private readonly PartnerEngine _partnerEngine = new();

        public DataTable GetPartners()
        {
            PartnerEngine partnerEngine = new PartnerEngine();
            return partnerEngine.GetPartners();
        }

        public DataTable GetVendors(bool dummyValue = true)
        {
            PartnerEngine partnerEngine = new PartnerEngine();
            return partnerEngine.GetVendors(dummyValue);
        }

        public BindingList<Partner> GetVendors()
        {
            return _partnerEngine.GetVendors();
        }

        public Partner GetVendorByTaxIdNumber(string vendorCode)
        {
            return _partnerEngine.GetVendorByTaxIdNumber(vendorCode);
        }
    }
}

﻿using System.Data;
using DataLayer;
using EntityLayer.Auth;

namespace BusinessLayer
{
    public class UserLogic
    {
        private UserEngine _userEngine = new();

        public User Authenticate(string username, string password)
        {
            return _userEngine.Authenticate(username, password);
        }
    }
}
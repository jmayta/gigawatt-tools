﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityLayer;
using DataLayer;

namespace BusinessLayer
{
    public class OrderLogic
    {
        private readonly OrderEngine _orderEngine = new OrderEngine();


        public int InsertOrder(Order orderObjectForSave)
        {
            return _orderEngine.InsertOrder(orderObjectForSave);
        }
    }
}

﻿using DataLayer;
using EntityLayer;
using System.ComponentModel;

namespace BusinessLayer
{
    public class ProjectLogic
    {
        private readonly ProjectEngine _projectEngine = new ProjectEngine();
        
        public BindingList<Project> GetProjects(bool onlyActives = false)
        {
            return _projectEngine.GetProjects(onlyActives);
        }

        public Project GetProjectByCode(string code, bool onlyActive = false)
        {
            return _projectEngine.GetProjectByCode(code, onlyActive);
        }
    }
}

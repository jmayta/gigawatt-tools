﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer;
using EntityLayer;
using System.ComponentModel;

namespace BusinessLayer
{
    public class ThingLogic
    {

        private readonly ThingEngine _thingEngine = new ThingEngine();

        public List<Thing> GetThings()
        {
            return _thingEngine.GetThings();
        }

        public Thing GetThingByCode(string code)
        {
            return _thingEngine.GetThingByCode(code);
        }

    }
}

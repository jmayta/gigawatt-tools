﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer;
using EntityLayer.Universal;

namespace BusinessLayer
{
    public class CurrencyLogic
    {
        private readonly CurrencyEngine _currencyEngine = new CurrencyEngine();

        public List<Currency> GetCurrencyList()
        {
            return _currencyEngine.GetCurrencyList();
        }
    }
}

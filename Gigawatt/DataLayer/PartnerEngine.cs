﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataLayer.Database;
using System.ComponentModel;
using EntityLayer;
using Microsoft.Data.SqlClient;

namespace DataLayer
{
    public class PartnerEngine
    {
        private DatabaseHelper dbHelper = new DatabaseHelper();

        public DataTable GetCustomers()
        {
            return dbHelper
                .ExecuteFill(
                    "Customers",
                    "usp_Partners_GeCustomers",
                    CommandType.StoredProcedure
                );
        }
        public DataTable GetPartners()
        {
            return dbHelper
                .ExecuteFill(
                    "Partners",
                    "Partners_GetPartners",
                    CommandType.StoredProcedure
                );
        }

        public DataTable GetVendors(bool dummyValue = true)
        {
            return dbHelper
                .ExecuteFill(
                    "Vendors",
                    "usp_Partners_GetVendors",
                    CommandType.StoredProcedure
                    );
        }

        public Partner GetVendorByTaxIdNumber(string taxIdNumber)
        {
            Partner partnerFound = new Partner();

            #region Parameters
            // Parameters
            SqlParameter taxIdNumberParameter = new SqlParameter
            {
                ParameterName = "@taxIdNumber",
                SqlDbType = SqlDbType.VarChar,
                Size = 25,
                Value = taxIdNumber
            };
            // End Parameters
            #endregion

            using (SqlDataReader reader = DatabaseHelper.ExecuteReader(
                    "usp_Partners_GetVendorByTaxIdNumber",
                    CommandType.StoredProcedure,
                    taxIdNumberParameter
                ))
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        partnerFound.Alias = reader["alias"] as string ?? null;
                        partnerFound.BusinessName = (string)reader["business_name"];
                        partnerFound.CountryId = reader["country_id"] as int? ?? null;
                        partnerFound.CreatedAt = (DateTime)reader["created_at"];
                        partnerFound.DeactivationDate = 
                            reader["deactivation_date"] as DateTime? ?? null;
                        partnerFound.Email = reader["email"] as string ?? null;
                        partnerFound.FiscalAddress = 
                            reader["fiscal_address"] as string ?? null;
                        partnerFound.Id = (int)reader["id"];
                        partnerFound.IsActive = (bool)reader["is_active"];
                        partnerFound.IsBank = reader["is_bank"] as bool? ?? false;
                        partnerFound.IsCustomer = (bool)reader["is_customer"];
                        partnerFound.IsVendor = (bool)reader["is_vendor"];
                        partnerFound.Location = reader["location"] as string ?? null;
                        partnerFound.TaxIdNumber = (string)reader["tax_id_number"];
                        partnerFound.TradeName = reader["trade_name"] as string ?? null;
                        partnerFound.Ubigeo = reader["ubigeo"] as string ?? null;

                        break;
                    }
                }
            }
            return partnerFound;
        }

        public BindingList<Partner> GetVendors()
        {
            BindingList<Partner> partnertBList = new BindingList<Partner>();
            using (SqlDataReader reader = DatabaseHelper.ExecuteReader(
                "usp_Partners_GetVendors", CommandType.StoredProcedure
                )
            )
            {
                while (reader.Read())
                {
                    Partner partner = new Partner
                    {
                        Id = (int?)reader["id"],
                        TaxIdNumber = (string)reader["tax_id_number"],
                        BusinessName = (string)reader["business_name"],
                        TradeName = reader["trade_name"] as string ?? null,
                        Alias = reader["alias"] as string ?? null,
                        FiscalAddress = reader["fiscal_address"] as string ?? null,
                        Location = reader["location"] as string ?? null,
                        Ubigeo = reader["ubigeo"] as string ?? null,
                        CountryId = reader["id"] as int? ?? null,
                        IsActive = (bool)reader["is_active"],
                        IsVendor = (bool)reader["is_vendor"],
                        IsCustomer = (bool)reader["is_customer"],
                        IsBank = (bool)reader["is_bank"],
                        Email = reader["email"] as string ?? null,
                        DeactivationDate = reader["deactivation_date"] as DateTime? ?? null,
                        CreatedAt = (DateTime)reader["created_at"]
                    };
                    partnertBList.Add(partner);
                }
            }
            
            return partnertBList;
        }
    }
}

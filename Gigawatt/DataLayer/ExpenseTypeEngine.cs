﻿using DataLayer.Database;
using EntityLayer.Universal;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class ExpenseTypeEngine
    {
        private readonly DatabaseHelper _dbHelper = new DatabaseHelper();

        public List<ExpenseType> GetExpenseTypes()
        {
            List<ExpenseType> expenseTypeList = new List<ExpenseType>();
            SqlDataReader reader = DatabaseHelper.ExecuteReader(
                "usp_ExpenseTypes_GetExpenseTypes",
                CommandType.StoredProcedure
                );

            using (reader)
            {
                while (reader.Read())
                {
                    ExpenseType expenseType = new ExpenseType
                    {
                        Id = (int?)reader["id"],
                        AlphaCode = reader["alpha_code"] == DBNull.Value ? 
                            null : (string)reader["alpha_code"],
                        Description = (string)reader["description"],
                        ParentId = reader["parent_id"] == DBNull.Value ? 
                            null : (int)reader["parent_id"]
                    };
                    expenseTypeList.Add(expenseType);
                }
            }
            return expenseTypeList;
        }
    }
}

﻿using DataLayer.Database;
using EntityLayer.Auth;
using Microsoft.Data.SqlClient;
using System.Data;

namespace DataLayer
{
    public class UserEngine
    {
        private DatabaseHelper _dbHelper = new DatabaseHelper();

        public DataTable GetUsersList()
        {
            // TODO: Crea procedimientos para manejo de Usuarios
            DataTable users = _dbHelper.ExecuteFill(
                "users", "sp_listar_usuarios", CommandType.StoredProcedure
                );
            return users;
        }

        public User Authenticate(string username, string password)
        {
            User user = new User();

            #region Parameters
            // Parameters
            SqlParameter usernameParameter = new SqlParameter
            {
                ParameterName = "@username",
                SqlDbType = SqlDbType.VarChar,
                Size = 200,
                Value = username
            };

            SqlParameter passwordParameter = new SqlParameter { 
                ParameterName = "@password", 
                SqlDbType = SqlDbType.VarChar, 
                Size = 250,
                Value = password
            };
            // End Parameters
            #endregion

            using (SqlDataReader reader = DatabaseHelper
                .ExecuteReader(
                    "usp_Users_AuthenticateUser",
                    CommandType.StoredProcedure,
                    usernameParameter,
                    passwordParameter
                )
            )
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        user = new User
                        {
                            Id = reader["id"] as int? ?? null,
                            Username = (string)reader["username"],
                            FirstName = reader["first_name"] as string ?? null,
                            LastName = reader["last_name"] as string ?? null,
                            Email = (string)reader["email"],
                            IsActive = (bool)reader["is_active"],
                            LastLogin = reader["last_login"] as DateTime? ?? null,
                            IsAuthenticated = reader["is_authenticated"] as bool? ?? null,
                            CreatedAt = (DateTime)reader["created_at"]
                        };
                        // TODO: Activar trigger que actualice el campo
                        // (is_authenticated)
                        // TODO: Activar trigger que actualice el campo (last_login)
                        // a la fecha y hora de éste inicio de sesión
                        break;
                    }
                }
                return user;
            }
        }

        private void UpdateUserLastLogin(int userId)
        {

        }

        private void ToggleAuthentication(int userId)
        {

        }

        private void ForceCloseUserAuthentication(int userId)
        {

        }
    }
}
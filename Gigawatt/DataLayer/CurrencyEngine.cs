﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

using DataLayer.Database;
using EntityLayer.Universal;
using Microsoft.Data.SqlClient;

namespace DataLayer
{
    public class CurrencyEngine
    {
        private readonly DatabaseHelper _dbHelper = new DatabaseHelper();

        public List<Currency> GetCurrencyList()
        {
            List<Currency> currencyList = new List<Currency>();
            SqlDataReader reader = DatabaseHelper.ExecuteReader(
                    "usp_Currencies_GetCurrencies", 
                    CommandType.StoredProcedure
                    );

            using (reader)
            {
                while(reader.Read())
                {
                    Currency currency = new Currency
                    {
                        Id = (int?)reader["id"],
                        Name = (string)reader["name"],
                        PluralName = reader["plural_name"] == DBNull.Value ?
                            null : (string?)reader["plural_name"],
                        Code = (string)reader["code"],
                        Symbol = reader["symbol"] == DBNull.Value ? 
                            null : (string?)reader["symbol"]
                    };
                    currencyList.Add(currency);
                }
            }

            return currencyList;
        }
    }
}

﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Reflection;
using System.Diagnostics;

namespace DataLayer.Database
{
    /// <summary>
    /// Class <c>DatabaseHelper</c> reusa el flujo genérico para el uso de SqlCommand.
    /// </summary>
    public class DatabaseHelper
    {
        #region Private Variables
        private string _dbErrorMessage = string.Empty;
        private static string _connectionString = ConfigurationManager.ConnectionStrings["csGigawatt"].ConnectionString;
        #endregion

        #region Public Variables
        public static string ConnectionString { get => _connectionString; }
        public string DbErrorMessage { get => _dbErrorMessage; set => _dbErrorMessage = value; }
        #endregion

        // Constructor
        public DatabaseHelper()
        {
            DbErrorMessage = String.Empty;
        }

        // Functions
        #region Functions

        /// <summary>
        /// Method <c>ExecuteNonQuery</c> ejecuta una consulta o un procedimiento 
        /// almacenado.
        /// </summary>
        /// <param name="commandText">
        /// Cadena que representa una `Consulta SQL` o un el nombre de un `Procedimiento Almacenado.`
        /// </param>
        /// <returns>
        /// El número de filas afectadas.
        /// </returns>
        public int ExecuteNonQuery(string commandText, CommandType commandType, params SqlParameter[] sqlParameters)
        {
            // Inicializa variable que almacenará la respuesta de ejecución del command
            int rowsAffected = 0;

            try
            {
                // Usa conexión de la instancia actual
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    // Usa commando 
                    using (SqlCommand command = new SqlCommand(commandText, connection))
                    {
                        command.Parameters.AddRange(sqlParameters);
                        command.CommandType = commandType;

                        connection.Open();
                        rowsAffected = command.ExecuteNonQuery();
                        return rowsAffected;
                    }
                };
            }
            catch (SqlException sqlEx)
            {
                DbErrorMessage = sqlEx.Message;
                Debug.WriteLine(sqlEx.Message);
                return rowsAffected;
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"{ex.TargetSite} : {ex.Message}");
                return rowsAffected;
            }
        }

        // ExecuteScalar
        public object ExecuteScalar(string commandText, CommandType commandType, params SqlParameter[] sqlParameters)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(commandText, connection))
                    {
                        command.CommandType = commandType;
                        command.Parameters.AddRange(sqlParameters);

                        connection.Open();
                        return command.ExecuteScalar(); ;
                    }
                };
            }
            catch (SqlException sqlEx)
            {
                this.DbErrorMessage = sqlEx.Message;

                return new object();
            }
            catch (Exception ex)
            {
                // TODO: Save exceptions in app log
                return new object();
                throw;

            }
        }
        // ExecuteFill
        public DataTable ExecuteFill(string dataTableName, string commandText, CommandType commandType)
        {
            DataTable dt = new DataTable(dataTableName);

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(commandText, connection))
                    {
                        command.CommandType = commandType;
                        using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                        {
                            adapter.Fill(dt);
                            return dt;
                        }
                    }
                };
            }
            catch (SqlException sqlEx)
            {
                this.DbErrorMessage = sqlEx.Message;

                return dt;
            }
            catch (Exception ex)
            {
                // TODO: Save exceptions in app log
                Debug.WriteLine($"{ex.TargetSite} : {ex.Message}");
                return dt;
            }
        }

        public List<T> ExecuteReader<T>(string commandText, CommandType commandType) where T : new()
        {
            // TODO: (PRO) Implementar ExecuteReader usando generics
            // NOTA:    ESTO SOLO FUNCIONA SIEMPRE EN CUANDO EL ORDEN Y LA CANTIDAD DE
            //          COLUMNAS DEVUELTAS DE LA BD CORRESPONDEN AL ORDEN Y LA CANTIDAD
            //          DE PROPIEDADES DECLARADAS DENTRO DE LA CLASE
            //          Estas propiedades deben ser públicas con su get y set respectivo;
            // Generar una lista de Entidades
            List<T> entitiesList = new List<T>();
            // Genera un objecto Entidad
            T entity = new T();
            // Obtener el tipo de entidad
            Type entityType = entity.GetType();
            // Ejecutar commandText
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(commandText, connection))
                {
                    command.CommandType = commandType;
                    connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        // Comparar la cantidad de filas resultantes de la consulta con la cantidad
                        // de propiedades (públicas) de la Entidad
                        if (reader.HasRows)
                        {
                            if(
                                reader.FieldCount == entityType.GetProperties()
                                .Where(property => property.CanWrite).Count()
                                )
                            {
                                // Si: ambas coinciden empezar el proceso de llenado
                                // Para el proceso de llenado:
                                PropertyInfo[] propertyEntityList = entityType.GetProperties();
                                // Iterar los resultados fila a fila con Read()
                                while (reader.Read())
                                {
                                    entity = new T();
                                    // Iterar cada columna de la fila `read`
                                    for (int columnIndex = 0; columnIndex < reader.FieldCount; columnIndex++)
                                    {
                                        // Asignar el valor de la posicion del reader a la propiedad en la misma posición
                                        propertyEntityList[columnIndex].SetValue(entity, reader[columnIndex]);
                                    }
                                    // Agregar la Entidad con las propiedades ya llenadas a la ListaDeEntidades
                                    entitiesList.Add(entity);
                                }
                                // Terminando la iteración de resultados obtenidos
                                // devolver ListaDeEntidades
                                return entitiesList;
                            }
                        }
                    }
                }
            }
            // De lo contrario: Devolver una lista vacía y/o un mensaje de error, o 
            // lanzar un error
            return new List<T>();
        }

        public List<object> ExecuteReader(string commandText, CommandType commandType)
        {
            List<object> objectList = new List<object>();
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(commandText, connection))
                {
                    command.CommandType = commandType;
                    connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            object[] itemList = new object[reader.FieldCount];
                            reader.GetValues(itemList);
                            objectList.Add(itemList);
                        }
                        return objectList;
                    }
                }
            }
        }

        public static SqlDataReader ExecuteReader(
            string commandText,
            CommandType commandType,
            params SqlParameter[] parameterCollection
            )
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            using (SqlCommand command = new SqlCommand(commandText, connection))
            {
                if (parameterCollection.Length > 0)
                {
                    command.Parameters.AddRange(parameterCollection);
                }
                command.CommandType = commandType;
                connection.Open();
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                return reader;
            }
        }
        #endregion
    }
}

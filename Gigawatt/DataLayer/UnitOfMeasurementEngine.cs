﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Database;
using EntityLayer.Universal;


namespace DataLayer
{
    public class UnitOfMeasurementEngine
    {
        private DatabaseHelper _dbHelper = new DatabaseHelper();

        public List<UnitOfMeasurement> GetUoms(bool withChild = true)
        {
            List<UnitOfMeasurement> uomList = new List<UnitOfMeasurement>();

            using (SqlDataReader reader = DatabaseHelper.ExecuteReader(
                "usp_UnitsOfMeasurement_GetUoms",
                CommandType.StoredProcedure)
                )
            {
                while (reader.Read())
                {
                    UnitOfMeasurement uom = new UnitOfMeasurement()
                    {
                        Id = (int?)reader["id"],
                        Name = (string)reader["name"],
                        NamePlural = reader["name_plural"] == DBNull.Value ?
                            null : (string)reader["name_plural"],
                        Abbreviation = (string)reader["abbreviation"],
                        NumericalValue = reader["numerical_value"] == DBNull.Value ?
                            null : (double?)reader["numerical_value"],
                        BaseUnitId = reader["base_unit"] == DBNull.Value ?
                            null : (int?)reader["base_unit"]
                    };
                    if (withChild)
                    {
                        if (uom.BaseUnitId != null && uom.BaseUnitId != 0)
                        {
                            uom.BaseUnit = GetUomById((int)uom.BaseUnitId);
                        }
                        uom.BaseUnit = null;
                    }
                    uomList.Add(uom);
                }
            }
            return uomList;
        }

        public UnitOfMeasurement GetUomById(int uomId)
        {
            UnitOfMeasurement uomFound = new UnitOfMeasurement();

            #region Parameters Declaration
            // Parameters
            SqlParameter uomIdParameter = new SqlParameter
            {
                ParameterName = "@id",
                SqlDbType = SqlDbType.Int,
                Value = uomId
            };
            // End Parameters
            #endregion
            
            using (SqlDataReader reader = DatabaseHelper.ExecuteReader(
                "usp_UnitsOfMeasurement_GetUom",
                CommandType.StoredProcedure,
                uomIdParameter)
                )
            {
                while (reader.Read())
                {
                    uomFound = new UnitOfMeasurement
                    {
                        Id = (int?)reader["id"],
                        Name = (string)reader["name"],
                        NamePlural = reader["name_plural"] == DBNull.Value ?
                            null : (string)reader["name_plural"],
                        Abbreviation = (string)reader["abbreviation"],
                        NumericalValue = reader["numerical_value"] == DBNull.Value ?
                            null : (int)reader["numerical_value"],
                        BaseUnitId = reader["base_unit"] == DBNull.Value ?
                            null : (int?)reader["base_unit"]
                    };
                    break;
                }
            }
            return uomFound;
        }

        public int InsertUom(UnitOfMeasurement uomObject)
        {
            int rowsAffected = 0;

            #region Parameters Declaration
            // Parameters
            SqlParameter uomNameParameter = new SqlParameter
            {
                ParameterName = "@name",
                SqlDbType = SqlDbType.VarChar,
                Size = 250,
                Value = uomObject.Name
            };
            SqlParameter uomNamePluralParameter = new SqlParameter
            {
                ParameterName = "@namePlural",
                SqlDbType = SqlDbType.VarChar,
                Size = 250,
                Value = uomObject.NamePlural
            };
            SqlParameter uomAbbreviationParameter = new SqlParameter
            {
                ParameterName = "@abbreviation",
                SqlDbType = SqlDbType.VarChar,
                Size = 5,
                Value = uomObject.Abbreviation
            };
            SqlParameter uomNumericalValueParameter = new SqlParameter
            {
                ParameterName = "@numericalValue",
                SqlDbType = SqlDbType.Float,
                Value = uomObject.NumericalValue
            };
            SqlParameter uomBaseUnitParameter = new SqlParameter
            {
                ParameterName = "@baseUnit",
                SqlDbType = SqlDbType.Int,
                Value = uomObject.BaseUnitId
            };

            SqlParameter[] sqlParameters =
            {
                uomNameParameter,
                uomNamePluralParameter,
                uomAbbreviationParameter,
                uomNumericalValueParameter,
                uomBaseUnitParameter
            };
            // End Parameters
            #endregion

            rowsAffected = _dbHelper.ExecuteNonQuery(
                "usp_UnitsOfMeasurement_InsertUom",
                CommandType.StoredProcedure,
                sqlParameters
                );

            return rowsAffected;
        }

        public int UpdateUom(UnitOfMeasurement uomObject)
        {
            int rowsAffected = 0;

            SqlParameter uomIdParameter = new SqlParameter
            {
                ParameterName = "@id",
                SqlDbType = SqlDbType.Int,
                Value = uomObject.Id
            };
            SqlParameter uomNameParameter = new SqlParameter
            {
                ParameterName = "@name",
                SqlDbType = SqlDbType.VarChar,
                Size = 250,
                Value = uomObject.Name
            };
            SqlParameter uomNamePluralParameter = new SqlParameter
            {
                ParameterName = "@namePlural",
                SqlDbType = SqlDbType.VarChar,
                Size = 250,
                Value = uomObject.NamePlural
            };
            SqlParameter uomAbbreviationParameter = new SqlParameter
            {
                ParameterName = "@abbreviation",
                SqlDbType = SqlDbType.VarChar,
                Size = 5,
                Value = uomObject.Abbreviation
            };
            SqlParameter uomNumericalValueParameter = new SqlParameter
            {
                ParameterName = "@numericalValue",
                SqlDbType = SqlDbType.Float,
                Value = uomObject.NumericalValue
            };
            SqlParameter uomBaseUnitParameter = new SqlParameter
            {
                ParameterName = "@baseUnit",
                SqlDbType = SqlDbType.Int,
                Value = uomObject.BaseUnitId
            };
            SqlParameter[] sqlParameters =
            {
                uomIdParameter,
                uomNameParameter,
                uomNamePluralParameter,
                uomAbbreviationParameter,
                uomNumericalValueParameter,
                uomBaseUnitParameter
            };

            rowsAffected = _dbHelper.ExecuteNonQuery(
                "usp_UnitsOfMeasurement_UpdateUom",
                CommandType.StoredProcedure,
                sqlParameters
                );

            return rowsAffected;
        }

        public int RemoveUom(UnitOfMeasurement uomObject)
        {
            SqlParameter uomIdParameter = new SqlParameter 
            { 
                ParameterName = "@id",
                SqlDbType = SqlDbType.Int,
                Value = uomObject.Id
            };

            return _dbHelper.ExecuteNonQuery(
                    "usp_UnitsOfMeasurement_RemoveUom",
                    CommandType.StoredProcedure,
                    uomIdParameter
                ); ;
        }
    }
}

﻿using DataLayer.Database;
using EntityLayer;
using Microsoft.Data.SqlClient;
using System.ComponentModel;
using System.Data;

namespace DataLayer
{
    public class ThingEngine
    {
        private readonly DatabaseHelper _dbHelper = new DatabaseHelper();

        public Thing GetThingByCode(string thingCode)
        {
            Thing thingFound = new Thing();
            #region Parameters
            // Parameters
            SqlParameter thingCodeParameter = new SqlParameter
            {
                ParameterName = "@code",
                SqlDbType = SqlDbType.VarChar,
                Size = 16,
                Value = thingCode
            };
            // End Parameters
            #endregion

            using (SqlDataReader reader = DatabaseHelper.ExecuteReader(
                    "usp_Things_GetThingByCode",
                    CommandType.StoredProcedure,
                    thingCodeParameter
                ))
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        thingFound.Id = (int)reader["id"];
                        thingFound.Code = (string)reader["code"];
                        thingFound.Unspsc = reader["unspsc"] == DBNull.Value ?
                            null : (string)reader["unspsc"];
                        thingFound.Description = (string)reader["description"];
                        thingFound.DefaultUom = reader["default_uom"] == DBNull.Value ?
                            null : (int?)reader["default_uom"];
                        thingFound.Kind = (string)reader["kind"];
                        thingFound.PicturePath = reader["picture_path"] == DBNull.Value ?
                            null : (string?)reader["picture_path"];
                        thingFound.BlueprintPath = reader["blueprint_path"] == DBNull.Value ?
                            null : (string?)reader["blueprint_path"];
                        thingFound.ManualPath = reader["manual_path"] == DBNull.Value ?
                            null : (string?)reader["manual_path"];
                        thingFound.Weight = reader["weight"] == DBNull.Value ?
                            null : (double?)reader["weight"];

                        break;
                    }
                }
            }
            return thingFound;
        }

        public List<Thing> GetThings()
        {
            List<Thing> thingList = new List<Thing>();

            using (SqlDataReader reader = DatabaseHelper
                .ExecuteReader(
                    "usp_Things_GetThings",
                    CommandType.StoredProcedure
                    )
                )
            {
                while (reader.Read())
                {
                    Thing thing = new Thing
                    {
                        Id = (int)reader["id"],
                        Code = (string)reader["code"],
                        Unspsc = reader["unspsc"] != DBNull.Value ?
                            (string)reader["unspsc"] : null,
                        Description = (string)reader["description"],
                        DefaultUom = reader["default_uom"] != DBNull.Value ?
                            (int?)reader["default_uom"] : null,
                        Kind = (string)reader["kind"]
                    };
                    thingList.Add(thing);
                }
            }
            return thingList;
        }

        //public BindingList<Thing> GetThings(bool bindingList)
        //{
        //    BindingList<Thing> thingList = new BindingList<Thing>();

        //    using (SqlDataReader reader = DatabaseHelper
        //        .ExecuteReader(
        //            _dbHelper.ConnectionString,
        //            "usp_Things_GetThings",
        //            CommandType.StoredProcedure
        //            )
        //        )
        //    {
        //        while (reader.Read())
        //        {
        //            Thing thing = new Thing
        //            {
        //                Id = (int)reader["id"],
        //                Code = (string)reader["code"],
        //                Unspsc = reader["unspsc"] != DBNull.Value ?
        //                    (string)reader["unspsc"] : null,
        //                Description = (string)reader["description"],
        //                DefaultUom = reader["default_uom"] != DBNull.Value ?
        //                    (int?)reader["default_uom"] : null,
        //                Kind = (string)reader["description"]
        //            };
        //            thingList.Add(thing);
        //        }
        //    }
        //    return thingList;
        //}
    }
}

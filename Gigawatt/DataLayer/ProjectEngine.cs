﻿using DataLayer.Database;
using EntityLayer;
using Microsoft.Data.SqlClient;
using System.ComponentModel;
using System.Data;

namespace DataLayer
{
    public class ProjectEngine
    {
        private readonly DatabaseHelper _dbHelper = new DatabaseHelper();

        public Project GetProjectByCode(string projectCode, bool onlyActive = false)
        {
            Project projectFound = new Project();

            #region Parameters
            // Parameters
            SqlParameter projectCodeParameter = new SqlParameter
            {
                ParameterName = "@code",
                SqlDbType = SqlDbType.VarChar,
                Size = 100,
                Value = projectCode
            };
            // End Parameters
            #endregion

            string commandText = "usp_Projects_GetProjectByCode";

            if (onlyActive)
            {
                commandText = "usp_Projects_GetActiveProjectByCode";
            }
            using (SqlDataReader reader = DatabaseHelper.ExecuteReader(
                    commandText,
                    CommandType.StoredProcedure,
                    projectCodeParameter
                ))
            {
                if (reader.HasRows)
                {
                    while(reader.Read())
                    {
                        projectFound = new Project 
                        {
                            Id = (int)reader["id"],
                            Code = (string)reader["code"],
                            Name = (string)reader["name"],
                            IsOwned = (bool)reader["is_owned"],
                            AwardDate = reader["award_date"] as DateTime? ?? null,
                            Budget = (decimal)reader["budget"],
                            CustomerId = (int)reader["customer_id"],
                            ParentId = reader["parent_id"] as int? ?? null,
                            IsActive = (bool)reader["is_active"],
                            CreatedAt = (DateTime)reader["created_at"]
                        };

                        break;
                    }
                }

            }
            return projectFound;
        }

        public BindingList<Project> GetProjects(bool onlyActives = false)
        {
            BindingList<Project> projectList = new BindingList<Project>();

            string commandText = "usp_Projects_GetProjects";

            if (onlyActives)
            {
                commandText = "usp_Projects_GetActiveProjects";
            }

            using (SqlDataReader reader = DatabaseHelper
                    .ExecuteReader(commandText, CommandType.StoredProcedure)
                    )
            {
                while (reader.Read())
                {
                    Project project = new Project
                    {
                        Id = (int)reader["id"],
                        Code = (string)reader["code"],
                        Name = (string)reader["name"],
                        IsOwned = (bool)reader["is_owned"],
                        AwardDate = reader["award_date"] as DateTime? ?? null,
                        Budget = (decimal)reader["budget"],
                        CustomerId = (int)reader["customer_id"],
                        ParentId = reader["parent_id"] as int? ?? null,
                        IsActive = (bool)reader["is_active"],
                        CreatedAt = (DateTime)reader["created_at"]
                    };
                    projectList.Add(project);
                }
            }
            return projectList;
        }
    }
}

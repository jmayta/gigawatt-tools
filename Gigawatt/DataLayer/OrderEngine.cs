﻿using DataLayer.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityLayer;
using System.Data;
using Microsoft.Data.SqlClient;

namespace DataLayer
{
    public class OrderEngine
    {
        private readonly DatabaseHelper _dbHelper = new DatabaseHelper();

        // Insert Order
        public int InsertOrder(Order orderForSave)
        {
            Order createdOrder = new Order();

            // Convert Order.OrderDetails list to DataTable for interaction with
            // the Table Type created on DB
            DataTable dtOrderDetails = new DataTable();
            dtOrderDetails.Columns.Add("external_code", typeof(string));
            dtOrderDetails.Columns.Add("demand", typeof(float));
            dtOrderDetails.Columns.Add("unit_price", typeof(decimal));
            dtOrderDetails.Columns.Add("value_order", typeof(decimal));
            dtOrderDetails.Columns.Add("thing_id", typeof(int));
            dtOrderDetails.Columns.Add("uom_id", typeof(int));

            foreach (OrderDetail orderDetail in orderForSave.OrderDetails!)
            {
                DataRow row = dtOrderDetails.NewRow();
                row["external_code"] = orderDetail.ExternalCode;
                row["demand"] = orderDetail.Demand;
                row["unit_price"] = orderDetail.UnitPrice;
                row["value_order"] = orderDetail.ValueOrder;
                row["thing_id"] = orderDetail.ThingObject.Id;
                row["uom_id"] = orderDetail.UomObject.Id;

                dtOrderDetails.Rows.Add(row);
            }

            #region Parameters Declaration
            SqlParameter externalDocument = new SqlParameter
            {
                ParameterName = "@externalDocument",
                SqlDbType = SqlDbType.VarChar,
                Size = 50,
                Value = orderForSave.ExternalDocument,
            };

            SqlParameter expenseTypeId = new SqlParameter
            {
                ParameterName = "@expenseTypeId",
                SqlDbType = SqlDbType.Int,
                Value = orderForSave.ExpenseTypeObject?.Id ?? null,
            };

            SqlParameter authorId = new SqlParameter
            {
                ParameterName = "@authorId",
                SqlDbType = SqlDbType.Int,
                Value = orderForSave.AuthorObject.Id,
            };

            SqlParameter amountUntaxed = new SqlParameter
            {
                ParameterName = "@amountUntaxed",
                SqlDbType = SqlDbType.Money,
                Value = orderForSave.AmountUntaxed,
            };

            SqlParameter amountTax = new SqlParameter
            {
                ParameterName = "@amountTax",
                SqlDbType = SqlDbType.Money,
                Value = orderForSave.AmountTax,
            };

            SqlParameter amountTotal = new SqlParameter
            {
                ParameterName = "@amountTotal",
                SqlDbType = SqlDbType.Money,
                Value = orderForSave.AmountTotal,
            };

            SqlParameter observation = new SqlParameter
            {
                ParameterName = "@observation",
                SqlDbType = SqlDbType.Text,
                Value = orderForSave.Observation,
            };

            SqlParameter status = new SqlParameter
            {
                ParameterName = "@status",
                SqlDbType = SqlDbType.VarChar,
                Size = 20,
                Value = orderForSave.Status,
            };

            SqlParameter termsConditions = new SqlParameter
            {
                ParameterName = "@termsConditions",
                SqlDbType = SqlDbType.Text,
                Value = orderForSave.TermsConditions,
            };

            SqlParameter currencyId = new SqlParameter
            {
                ParameterName = "@currencyId",
                SqlDbType = SqlDbType.Int,
                Value = orderForSave.CurrencyObject.Id,
            };

            SqlParameter projectId = new SqlParameter
            {
                ParameterName = "@projectId",
                SqlDbType = SqlDbType.Int,
                Value = orderForSave.ProjectObject.Id,
            };

            SqlParameter vendorId = new SqlParameter
            {
                ParameterName = "@vendorId",
                SqlDbType = SqlDbType.Int,
                Value = orderForSave.VendorObject.Id,
            };

            SqlParameter orderDetails = new SqlParameter
            {
                ParameterName = "@orderDetail",
                SqlDbType = SqlDbType.Structured,
                Value = dtOrderDetails
            };

            SqlParameter[] sqlParameters =
            {
                externalDocument,
                expenseTypeId,
                authorId,
                amountUntaxed,
                amountTax,
                amountTotal,
                observation,
                status,
                termsConditions,
                currencyId,
                projectId,
                vendorId,
                orderDetails
            };
            #endregion

            // agrega en base de datos
            int result = _dbHelper.ExecuteNonQuery(
                "usp_Orders_InsertOrder",
                CommandType.StoredProcedure,
                sqlParameters
                );
            
            if (result > 0 )
            {
                // TODO: (MED) PDF creator
                // TODO: (LIGHT) Send Email confirmation
            }
            return result;
        }

    }
}

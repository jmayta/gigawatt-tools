﻿namespace UI.Logistics.Orders
{
    partial class FrmListOrders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmListOrders));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsmiNewOrder = new System.Windows.Forms.ToolStripMenuItem();
            this.exportarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aExcelxlsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aPDFpdfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reporteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslOrdersCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.dgvOrders = new System.Windows.Forms.DataGridView();
            this.txtSearchOrder = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbFilter = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrders)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiNewOrder,
            this.exportarToolStripMenuItem,
            this.reporteToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(893, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tsmiNewOrder
            // 
            this.tsmiNewOrder.Image = global::UI.Properties.Resources.Hopstarter_Soft_Scraps_File_New_16;
            this.tsmiNewOrder.Name = "tsmiNewOrder";
            this.tsmiNewOrder.Size = new System.Drawing.Size(69, 20);
            this.tsmiNewOrder.Text = "&Nueva";
            // 
            // exportarToolStripMenuItem
            // 
            this.exportarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aExcelxlsToolStripMenuItem,
            this.aPDFpdfToolStripMenuItem});
            this.exportarToolStripMenuItem.Image = global::UI.Properties.Resources.Custom_Icon_Design_Flatastic_1_Export_16;
            this.exportarToolStripMenuItem.Name = "exportarToolStripMenuItem";
            this.exportarToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.exportarToolStripMenuItem.Text = "Exportar";
            // 
            // aExcelxlsToolStripMenuItem
            // 
            this.aExcelxlsToolStripMenuItem.Name = "aExcelxlsToolStripMenuItem";
            this.aExcelxlsToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.aExcelxlsToolStripMenuItem.Text = "A Excel (.xls)";
            // 
            // aPDFpdfToolStripMenuItem
            // 
            this.aPDFpdfToolStripMenuItem.Name = "aPDFpdfToolStripMenuItem";
            this.aPDFpdfToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.aPDFpdfToolStripMenuItem.Text = "A PDF (.pdf)";
            // 
            // reporteToolStripMenuItem
            // 
            this.reporteToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.reporteToolStripMenuItem.Image = global::UI.Properties.Resources._7479648_chart_graph_analytics_statistics_diagram_icon;
            this.reporteToolStripMenuItem.Name = "reporteToolStripMenuItem";
            this.reporteToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.reporteToolStripMenuItem.Text = "Reporte";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.tsslOrdersCount,
            this.toolStripStatusLabel3});
            this.statusStrip1.Location = new System.Drawing.Point(0, 424);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(893, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(96, 17);
            this.toolStripStatusLabel1.Text = "Nro de Órdenes :";
            // 
            // tsslOrdersCount
            // 
            this.tsslOrdersCount.Font = new System.Drawing.Font("Roboto Mono", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.tsslOrdersCount.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsslOrdersCount.Name = "tsslOrdersCount";
            this.tsslOrdersCount.Size = new System.Drawing.Size(42, 17);
            this.tsslOrdersCount.Text = "1,235";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(19, 17);
            this.toolStripStatusLabel3.Text = "￤";
            // 
            // dgvOrders
            // 
            this.dgvOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOrders.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgvOrders.Location = new System.Drawing.Point(0, 103);
            this.dgvOrders.Name = "dgvOrders";
            this.dgvOrders.RowTemplate.Height = 25;
            this.dgvOrders.Size = new System.Drawing.Size(893, 321);
            this.dgvOrders.TabIndex = 2;
            // 
            // txtSearchOrder
            // 
            this.txtSearchOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearchOrder.Location = new System.Drawing.Point(545, 63);
            this.txtSearchOrder.Name = "txtSearchOrder";
            this.txtSearchOrder.Size = new System.Drawing.Size(336, 23);
            this.txtSearchOrder.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(494, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Buscar:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbFilter
            // 
            this.cmbFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbFilter.FormattingEnabled = true;
            this.cmbFilter.Location = new System.Drawing.Point(63, 63);
            this.cmbFilter.Name = "cmbFilter";
            this.cmbFilter.Size = new System.Drawing.Size(142, 23);
            this.cmbFilter.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Filtrar:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmListPurchases
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(893, 446);
            this.Controls.Add(this.cmbFilter);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSearchOrder);
            this.Controls.Add(this.dgvOrders);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmListPurchases";
            this.Text = "Lista de Compras";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrders)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MenuStrip menuStrip1;
        private StatusStrip statusStrip1;
        private DataGridView dgvOrders;
        private ToolStripMenuItem tsmiNewOrder;
        private TextBox txtSearchOrder;
        private Label label1;
        private ToolStripStatusLabel toolStripStatusLabel1;
        private ToolStripStatusLabel tsslOrdersCount;
        private ToolStripStatusLabel toolStripStatusLabel3;
        private ComboBox cmbFilter;
        private Label label2;
        private ToolStripMenuItem exportarToolStripMenuItem;
        private ToolStripMenuItem aExcelxlsToolStripMenuItem;
        private ToolStripMenuItem aPDFpdfToolStripMenuItem;
        private ToolStripMenuItem reporteToolStripMenuItem;
    }
}
﻿namespace UI
{
    partial class ModVendor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModVendor));
            cmbFilter = new ComboBox();
            txtSearch = new TextBox();
            dgvVendors = new DataGridView();
            dgvcId = new DataGridViewTextBoxColumn();
            dgvcTaxIdNumber = new DataGridViewTextBoxColumn();
            dgvcBusinessName = new DataGridViewTextBoxColumn();
            statusStrip1 = new StatusStrip();
            toolStripStatusLabel3 = new ToolStripStatusLabel();
            tsslItemsFound = new ToolStripStatusLabel();
            toolStripStatusLabel2 = new ToolStripStatusLabel();
            tsslUser = new ToolStripStatusLabel();
            label2 = new Label();
            label1 = new Label();
            btnCancel = new Button();
            btnAccept = new Button();
            ((System.ComponentModel.ISupportInitialize)dgvVendors).BeginInit();
            statusStrip1.SuspendLayout();
            SuspendLayout();
            // 
            // cmbFilter
            // 
            cmbFilter.FormattingEnabled = true;
            cmbFilter.Location = new Point(12, 30);
            cmbFilter.Name = "cmbFilter";
            cmbFilter.Size = new Size(110, 23);
            cmbFilter.TabIndex = 1;
            // 
            // txtSearch
            // 
            txtSearch.Location = new Point(142, 30);
            txtSearch.Name = "txtSearch";
            txtSearch.PlaceholderText = "Escriba aquí";
            txtSearch.Size = new Size(290, 23);
            txtSearch.TabIndex = 0;
            txtSearch.TextChanged += txtSearch_TextChanged;
            // 
            // dgvVendors
            // 
            dgvVendors.AllowUserToAddRows = false;
            dgvVendors.AllowUserToDeleteRows = false;
            dgvVendors.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvVendors.Columns.AddRange(new DataGridViewColumn[] { dgvcId, dgvcTaxIdNumber, dgvcBusinessName });
            dgvVendors.Dock = DockStyle.Top;
            dgvVendors.Location = new Point(0, 71);
            dgvVendors.MultiSelect = false;
            dgvVendors.Name = "dgvVendors";
            dgvVendors.ReadOnly = true;
            dgvVendors.RowTemplate.Height = 25;
            dgvVendors.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvVendors.Size = new Size(444, 288);
            dgvVendors.TabIndex = 2;
            dgvVendors.CellDoubleClick += dgvVendors_CellDoubleClick;
            // 
            // dgvcId
            // 
            dgvcId.HeaderText = "Id";
            dgvcId.Name = "dgvcId";
            dgvcId.ReadOnly = true;
            dgvcId.Visible = false;
            // 
            // dgvcTaxIdNumber
            // 
            dgvcTaxIdNumber.FillWeight = 35F;
            dgvcTaxIdNumber.HeaderText = "Código";
            dgvcTaxIdNumber.Name = "dgvcTaxIdNumber";
            dgvcTaxIdNumber.ReadOnly = true;
            // 
            // dgvcBusinessName
            // 
            dgvcBusinessName.HeaderText = "Rz. Social";
            dgvcBusinessName.Name = "dgvcBusinessName";
            dgvcBusinessName.ReadOnly = true;
            // 
            // statusStrip1
            // 
            statusStrip1.Items.AddRange(new ToolStripItem[] { toolStripStatusLabel3, tsslItemsFound, toolStripStatusLabel2, tsslUser });
            statusStrip1.Location = new Point(0, 403);
            statusStrip1.Name = "statusStrip1";
            statusStrip1.Size = new Size(444, 22);
            statusStrip1.TabIndex = 7;
            statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel3
            // 
            toolStripStatusLabel3.Font = new Font("Segoe UI", 8.25F, FontStyle.Bold, GraphicsUnit.Point);
            toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            toolStripStatusLabel3.Size = new Size(106, 17);
            toolStripStatusLabel3.Text = "Items Encontrados:";
            // 
            // tsslItemsFound
            // 
            tsslItemsFound.Name = "tsslItemsFound";
            tsslItemsFound.Size = new Size(46, 17);
            tsslItemsFound.Text = "[count]";
            // 
            // toolStripStatusLabel2
            // 
            toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            toolStripStatusLabel2.Size = new Size(224, 17);
            toolStripStatusLabel2.Spring = true;
            // 
            // tsslUser
            // 
            tsslUser.Font = new Font("Segoe UI", 8.25F, FontStyle.Bold, GraphicsUnit.Point);
            tsslUser.Image = (Image)resources.GetObject("tsslUser.Image");
            tsslUser.Name = "tsslUser";
            tsslUser.Size = new Size(53, 17);
            tsslUser.Text = "[user]";
            // 
            // label2
            // 
            label2.BackColor = SystemColors.ControlDarkDark;
            label2.Dock = DockStyle.Top;
            label2.Location = new Point(0, 0);
            label2.Name = "label2";
            label2.Size = new Size(444, 71);
            label2.TabIndex = 14;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.BackColor = SystemColors.ControlDarkDark;
            label1.ForeColor = SystemColors.HighlightText;
            label1.Location = new Point(12, 12);
            label1.Name = "label1";
            label1.Size = new Size(66, 15);
            label1.TabIndex = 15;
            label1.Text = "Buscar por:";
            // 
            // btnCancel
            // 
            btnCancel.Location = new Point(298, 365);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new Size(64, 32);
            btnCancel.TabIndex = 4;
            btnCancel.Text = "&Cancelar";
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnAccept
            // 
            btnAccept.DialogResult = DialogResult.OK;
            btnAccept.Location = new Point(368, 365);
            btnAccept.Name = "btnAccept";
            btnAccept.Size = new Size(64, 32);
            btnAccept.TabIndex = 3;
            btnAccept.Text = "&Aceptar";
            btnAccept.UseVisualStyleBackColor = true;
            btnAccept.Click += btnAccept_Click;
            // 
            // ModVendor
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(444, 425);
            Controls.Add(btnCancel);
            Controls.Add(btnAccept);
            Controls.Add(label1);
            Controls.Add(statusStrip1);
            Controls.Add(dgvVendors);
            Controls.Add(txtSearch);
            Controls.Add(cmbFilter);
            Controls.Add(label2);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            Icon = (Icon)resources.GetObject("$this.Icon");
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "ModVendor";
            SizeGripStyle = SizeGripStyle.Hide;
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Búsqueda de Proveedor";
            Load += ModVendor_Load;
            ((System.ComponentModel.ISupportInitialize)dgvVendors).EndInit();
            statusStrip1.ResumeLayout(false);
            statusStrip1.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private ComboBox cmbFilter;
        private TextBox txtSearch;
        private DataGridView dgvVendors;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel tsslItemsFound;
        private ToolStripStatusLabel toolStripStatusLabel2;
        private Label label2;
        private Label label1;
        private Button btnCancel;
        private Button btnAccept;
        private DataGridViewTextBoxColumn dgvcId;
        private DataGridViewTextBoxColumn dgvcTaxIdNumber;
        private DataGridViewTextBoxColumn dgvcBusinessName;
        private ToolStripStatusLabel tsslUser;
        private ToolStripStatusLabel toolStripStatusLabel3;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EntityLayer;
using DataLayer;
using UI.Utils;
using EntityLayer.Auth;

namespace UI
{
    public partial class ModProject : Form
    {
        private Project _projectObject;
        private User _authenticatedUser;
        private readonly ProjectEngine _projectEngine = new ProjectEngine();

        private BindingList<Project> _projectList;

        public ModProject()
        {
            InitializeComponent();
        }

        public ModProject(User authenticatedUser)
        {
            InitializeComponent();
            this._authenticatedUser = authenticatedUser;
        }

        public Project ProjectObject { get => _projectObject; }

        private void ModProject_Load(object sender, EventArgs e)
        {
            _projectList = _projectEngine.GetProjects();

            dgvProjects.AutoGenerateColumns = false;
            dgvProjects.Columns["dgvcId"].DataPropertyName = "Id";
            dgvProjects.Columns["dgvcCode"].DataPropertyName = "Code";
            dgvProjects.Columns["dgvcName"].DataPropertyName = "Name";

            dgvProjects.DataSource = _projectList;

            tsslCount.Text = _projectList.Count.ToString();
            tsslUser.Text = _authenticatedUser.Username ?? string.Empty;
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (txtSearch.Text.Trim() != string.Empty)
            {
                _projectList = FormUtils.FilterByProperty(
                    _projectList,
                    "Name",
                    ((TextBox)sender).Text);

                dgvProjects.DataSource = _projectList;
                tsslCount.Text = _projectList.Count.ToString();
            }
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            _projectObject = (Project)dgvProjects.CurrentRow.DataBoundItem;
            this.Close();
        }

        private void dgvProjects_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            _projectObject = (Project)dgvProjects.CurrentRow.DataBoundItem;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}

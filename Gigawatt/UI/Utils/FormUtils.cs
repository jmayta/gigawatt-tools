﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace UI.Utils
{
    internal class FormUtils
    {
        public static string BindProperty(object property, string propertyName)
        {
            string? retValue = "";

            if (propertyName.Contains('.'))
            {
                PropertyInfo[] arrayProperties;
                string leftPropertyName;

                leftPropertyName = propertyName.Substring(0, propertyName.IndexOf("."));
                arrayProperties = property.GetType().GetProperties();
                foreach (PropertyInfo propertyInfo in arrayProperties)
                {
                    if (propertyInfo.Name == leftPropertyName)
                    {
                        if (propertyInfo.GetValue(property) == null)
                        {
                            return retValue;
                        }
                        retValue = BindProperty(
                            propertyInfo.GetValue(property)!,
                            propertyName.Substring(propertyName.IndexOf('.') + 1)
                            );
                        break;
                    }
                }
            }
            else
            {
                Type propertyType;
                PropertyInfo propertyInfo;

                propertyType = property.GetType();
                propertyInfo = propertyType.GetProperty(propertyName)!;
                retValue = propertyInfo.GetValue(property)!.ToString();
            }

            return retValue!;
        }

        public static BindingList<T> FilterByProperty<T>(
            BindingList<T> objectList, string propertyName, string searchValue)
        {
            PropertyInfo property = typeof(T).GetProperty(propertyName)!;

            return new BindingList<T>(
                objectList.Where(
                    genericObject => 
                        property
                        .GetValue(genericObject)!
                        .ToString()!.ToLower()
                        .StartsWith(searchValue))
                .ToList()
                );
        }
    }
}

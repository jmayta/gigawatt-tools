using EntityLayer.Auth;
using UI.Logistics.Orders;

namespace UI
{
    public partial class FrmMain : Form
    {
        private User _authenticatedUser;


        public FrmMain(User authenticatedUser)
        {
            InitializeComponent();
            _authenticatedUser = authenticatedUser;
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            tsslUsername.Text = _authenticatedUser!.Username;
        }

        private void tsmiGenerarOrden_Click(object sender, EventArgs e)
        {
            FrmInsertOrder frmInsertOrder = new FrmInsertOrder(_authenticatedUser);
            frmInsertOrder.Show();
        }

        private void tsmiCurrencies_Click(object sender, EventArgs e)
        {
            FrmUoms frmUoms = new();
            frmUoms.Show();
        }
    }
}
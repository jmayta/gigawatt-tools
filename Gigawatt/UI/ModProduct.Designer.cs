﻿namespace UI
{
    partial class ModProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModProduct));
            btnAccept = new Button();
            dgvThings = new DataGridView();
            txtSearch = new TextBox();
            label1 = new Label();
            cmbFilter = new ComboBox();
            label2 = new Label();
            statusStrip1 = new StatusStrip();
            toolStripStatusLabel3 = new ToolStripStatusLabel();
            tsslCount = new ToolStripStatusLabel();
            toolStripStatusLabel2 = new ToolStripStatusLabel();
            tsslUser = new ToolStripStatusLabel();
            btnCancel = new Button();
            dgvcId = new DataGridViewTextBoxColumn();
            dgvcCode = new DataGridViewTextBoxColumn();
            dgvcDescription = new DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)dgvThings).BeginInit();
            statusStrip1.SuspendLayout();
            SuspendLayout();
            // 
            // btnAccept
            // 
            btnAccept.DialogResult = DialogResult.OK;
            btnAccept.Location = new Point(368, 365);
            btnAccept.Name = "btnAccept";
            btnAccept.Size = new Size(64, 32);
            btnAccept.TabIndex = 11;
            btnAccept.Text = "&Aceptar";
            btnAccept.UseVisualStyleBackColor = true;
            btnAccept.Click += btnAccept_Click;
            // 
            // dgvThings
            // 
            dgvThings.AllowUserToAddRows = false;
            dgvThings.AllowUserToDeleteRows = false;
            dgvThings.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvThings.Columns.AddRange(new DataGridViewColumn[] { dgvcId, dgvcCode, dgvcDescription });
            dgvThings.Dock = DockStyle.Top;
            dgvThings.Location = new Point(0, 71);
            dgvThings.MultiSelect = false;
            dgvThings.Name = "dgvThings";
            dgvThings.ReadOnly = true;
            dgvThings.RowTemplate.Height = 25;
            dgvThings.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvThings.Size = new Size(444, 288);
            dgvThings.TabIndex = 10;
            dgvThings.CellDoubleClick += dgvThings_CellDoubleClick;
            // 
            // txtSearch
            // 
            txtSearch.Location = new Point(142, 31);
            txtSearch.Name = "txtSearch";
            txtSearch.PlaceholderText = "Escriba aquí";
            txtSearch.Size = new Size(290, 23);
            txtSearch.TabIndex = 9;
            txtSearch.TextChanged += txtSearch_TextChanged;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.BackColor = SystemColors.ControlDarkDark;
            label1.ForeColor = SystemColors.HighlightText;
            label1.Location = new Point(12, 13);
            label1.Name = "label1";
            label1.Size = new Size(66, 15);
            label1.TabIndex = 8;
            label1.Text = "Buscar por:";
            // 
            // cmbFilter
            // 
            cmbFilter.FormattingEnabled = true;
            cmbFilter.Location = new Point(12, 31);
            cmbFilter.Name = "cmbFilter";
            cmbFilter.Size = new Size(110, 23);
            cmbFilter.TabIndex = 7;
            // 
            // label2
            // 
            label2.BackColor = SystemColors.ControlDarkDark;
            label2.Dock = DockStyle.Top;
            label2.Location = new Point(0, 0);
            label2.Name = "label2";
            label2.Size = new Size(444, 71);
            label2.TabIndex = 13;
            // 
            // statusStrip1
            // 
            statusStrip1.Items.AddRange(new ToolStripItem[] { toolStripStatusLabel3, tsslCount, toolStripStatusLabel2, tsslUser });
            statusStrip1.Location = new Point(0, 403);
            statusStrip1.Name = "statusStrip1";
            statusStrip1.Size = new Size(444, 22);
            statusStrip1.TabIndex = 14;
            statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel3
            // 
            toolStripStatusLabel3.Font = new Font("Segoe UI", 8.25F, FontStyle.Bold, GraphicsUnit.Point);
            toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            toolStripStatusLabel3.Size = new Size(106, 17);
            toolStripStatusLabel3.Text = "Items Encontrados:";
            // 
            // tsslCount
            // 
            tsslCount.Name = "tsslCount";
            tsslCount.Size = new Size(46, 17);
            tsslCount.Text = "[count]";
            // 
            // toolStripStatusLabel2
            // 
            toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            toolStripStatusLabel2.Size = new Size(224, 17);
            toolStripStatusLabel2.Spring = true;
            // 
            // tsslUser
            // 
            tsslUser.Font = new Font("Segoe UI", 8.25F, FontStyle.Bold, GraphicsUnit.Point);
            tsslUser.Image = (Image)resources.GetObject("tsslUser.Image");
            tsslUser.Name = "tsslUser";
            tsslUser.Size = new Size(53, 17);
            tsslUser.Text = "[user]";
            // 
            // btnCancel
            // 
            btnCancel.DialogResult = DialogResult.OK;
            btnCancel.Location = new Point(298, 365);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new Size(64, 32);
            btnCancel.TabIndex = 11;
            btnCancel.Text = "&Cancelar";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += btnAccept_Click;
            // 
            // dgvcId
            // 
            dgvcId.HeaderText = "Id";
            dgvcId.Name = "dgvcId";
            dgvcId.ReadOnly = true;
            dgvcId.Visible = false;
            // 
            // dgvcCode
            // 
            dgvcCode.FillWeight = 35F;
            dgvcCode.HeaderText = "Código";
            dgvcCode.Name = "dgvcCode";
            dgvcCode.ReadOnly = true;
            // 
            // dgvcDescription
            // 
            dgvcDescription.HeaderText = "Descripción";
            dgvcDescription.Name = "dgvcDescription";
            dgvcDescription.ReadOnly = true;
            // 
            // ModProduct
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(444, 425);
            Controls.Add(statusStrip1);
            Controls.Add(btnCancel);
            Controls.Add(btnAccept);
            Controls.Add(dgvThings);
            Controls.Add(txtSearch);
            Controls.Add(label1);
            Controls.Add(cmbFilter);
            Controls.Add(label2);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            Icon = (Icon)resources.GetObject("$this.Icon");
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "ModProduct";
            SizeGripStyle = SizeGripStyle.Hide;
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Búsqueda de Producto";
            Load += ModProduct_Load;
            ((System.ComponentModel.ISupportInitialize)dgvThings).EndInit();
            statusStrip1.ResumeLayout(false);
            statusStrip1.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnAccept;
        private DataGridView dgvThings;
        private TextBox txtSearch;
        private Label label1;
        private ComboBox cmbFilter;
        private Label label2;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel toolStripStatusLabel2;
        private ToolStripStatusLabel tsslUser;
        private ToolStripStatusLabel toolStripStatusLabel3;
        private ToolStripStatusLabel tsslCount;
        private Button btnCancel;
        private DataGridViewTextBoxColumn dgvcId;
        private DataGridViewTextBoxColumn dgvcCode;
        private DataGridViewTextBoxColumn dgvcDescription;
    }
}
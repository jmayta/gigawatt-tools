﻿namespace UI
{
    partial class ModProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModProject));
            btnCancel = new Button();
            btnAccept = new Button();
            dgvProjects = new DataGridView();
            dgvcId = new DataGridViewTextBoxColumn();
            dgvcCode = new DataGridViewTextBoxColumn();
            dgvcName = new DataGridViewTextBoxColumn();
            txtSearch = new TextBox();
            label1 = new Label();
            cmbFilter = new ComboBox();
            label2 = new Label();
            statusStrip1 = new StatusStrip();
            toolStripStatusLabel3 = new ToolStripStatusLabel();
            tsslCount = new ToolStripStatusLabel();
            toolStripStatusLabel2 = new ToolStripStatusLabel();
            tsslUser = new ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)dgvProjects).BeginInit();
            statusStrip1.SuspendLayout();
            SuspendLayout();
            // 
            // btnCancel
            // 
            btnCancel.Location = new Point(298, 365);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new Size(64, 32);
            btnCancel.TabIndex = 18;
            btnCancel.Text = "&Cancelar";
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnAccept
            // 
            btnAccept.DialogResult = DialogResult.OK;
            btnAccept.Location = new Point(368, 365);
            btnAccept.Name = "btnAccept";
            btnAccept.Size = new Size(64, 32);
            btnAccept.TabIndex = 19;
            btnAccept.Text = "&Aceptar";
            btnAccept.UseVisualStyleBackColor = true;
            btnAccept.Click += btnAccept_Click;
            // 
            // dgvProjects
            // 
            dgvProjects.AllowUserToAddRows = false;
            dgvProjects.AllowUserToDeleteRows = false;
            dgvProjects.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvProjects.Columns.AddRange(new DataGridViewColumn[] { dgvcId, dgvcCode, dgvcName });
            dgvProjects.Dock = DockStyle.Top;
            dgvProjects.Location = new Point(0, 71);
            dgvProjects.MultiSelect = false;
            dgvProjects.Name = "dgvProjects";
            dgvProjects.ReadOnly = true;
            dgvProjects.RowTemplate.Height = 25;
            dgvProjects.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvProjects.Size = new Size(444, 288);
            dgvProjects.TabIndex = 17;
            dgvProjects.CellDoubleClick += dgvProjects_CellDoubleClick;
            // 
            // dgvcId
            // 
            dgvcId.HeaderText = "Id";
            dgvcId.Name = "dgvcId";
            dgvcId.ReadOnly = true;
            dgvcId.Visible = false;
            // 
            // dgvcCode
            // 
            dgvcCode.FillWeight = 35F;
            dgvcCode.HeaderText = "Código";
            dgvcCode.Name = "dgvcCode";
            dgvcCode.ReadOnly = true;
            // 
            // dgvcName
            // 
            dgvcName.HeaderText = "Nombre";
            dgvcName.Name = "dgvcName";
            dgvcName.ReadOnly = true;
            // 
            // txtSearch
            // 
            txtSearch.Location = new Point(142, 30);
            txtSearch.Name = "txtSearch";
            txtSearch.PlaceholderText = "Escriba aquí";
            txtSearch.Size = new Size(290, 23);
            txtSearch.TabIndex = 16;
            txtSearch.TextChanged += txtSearch_TextChanged;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.BackColor = SystemColors.ControlDarkDark;
            label1.ForeColor = SystemColors.HighlightText;
            label1.Location = new Point(12, 12);
            label1.Name = "label1";
            label1.Size = new Size(66, 15);
            label1.TabIndex = 15;
            label1.Text = "Buscar por:";
            // 
            // cmbFilter
            // 
            cmbFilter.FormattingEnabled = true;
            cmbFilter.Location = new Point(12, 30);
            cmbFilter.Name = "cmbFilter";
            cmbFilter.Size = new Size(110, 23);
            cmbFilter.TabIndex = 14;
            // 
            // label2
            // 
            label2.BackColor = SystemColors.ControlDarkDark;
            label2.Dock = DockStyle.Top;
            label2.Location = new Point(0, 0);
            label2.Name = "label2";
            label2.Size = new Size(444, 71);
            label2.TabIndex = 20;
            // 
            // statusStrip1
            // 
            statusStrip1.Items.AddRange(new ToolStripItem[] { toolStripStatusLabel3, tsslCount, toolStripStatusLabel2, tsslUser });
            statusStrip1.Location = new Point(0, 403);
            statusStrip1.Name = "statusStrip1";
            statusStrip1.Size = new Size(444, 22);
            statusStrip1.TabIndex = 21;
            statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel3
            // 
            toolStripStatusLabel3.Font = new Font("Segoe UI", 8.25F, FontStyle.Bold, GraphicsUnit.Point);
            toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            toolStripStatusLabel3.Size = new Size(106, 17);
            toolStripStatusLabel3.Text = "Items Encontrados:";
            // 
            // tsslCount
            // 
            tsslCount.Name = "tsslCount";
            tsslCount.Size = new Size(46, 17);
            tsslCount.Text = "[count]";
            // 
            // toolStripStatusLabel2
            // 
            toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            toolStripStatusLabel2.Size = new Size(224, 17);
            toolStripStatusLabel2.Spring = true;
            // 
            // tsslUser
            // 
            tsslUser.Font = new Font("Segoe UI", 8.25F, FontStyle.Bold, GraphicsUnit.Point);
            tsslUser.Image = (Image)resources.GetObject("tsslUser.Image");
            tsslUser.Name = "tsslUser";
            tsslUser.Size = new Size(53, 17);
            tsslUser.Text = "[user]";
            // 
            // ModProject
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(444, 425);
            Controls.Add(statusStrip1);
            Controls.Add(btnCancel);
            Controls.Add(btnAccept);
            Controls.Add(dgvProjects);
            Controls.Add(txtSearch);
            Controls.Add(label1);
            Controls.Add(cmbFilter);
            Controls.Add(label2);
            Name = "ModProject";
            Text = "Búsqueda de Proyectos";
            Load += ModProject_Load;
            ((System.ComponentModel.ISupportInitialize)dgvProjects).EndInit();
            statusStrip1.ResumeLayout(false);
            statusStrip1.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnCancel;
        private Button btnAccept;
        private DataGridView dgvProjects;
        private TextBox txtSearch;
        private Label label1;
        private ComboBox cmbFilter;
        private Label label2;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel toolStripStatusLabel3;
        private ToolStripStatusLabel tsslCount;
        private ToolStripStatusLabel toolStripStatusLabel2;
        private ToolStripStatusLabel tsslUser;
        private DataGridViewTextBoxColumn dgvcId;
        private DataGridViewTextBoxColumn dgvcCode;
        private DataGridViewTextBoxColumn dgvcName;
    }
}
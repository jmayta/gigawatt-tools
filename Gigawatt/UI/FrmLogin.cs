﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EntityLayer.Auth;
using BusinessLayer;

namespace UI.Auth
{
    public partial class FrmLogin : Form
    {
        private readonly UserLogic _userLogic = new UserLogic();
        private User _authenticatedUser;

        // Functions
        private void ClearTextBoxes()
        {
            txtUser.Text = String.Empty;
            txtPassword.Text = String.Empty;
        }

        public FrmLogin()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult exit = MessageBox.Show(
                "¿Realmente quiere cerrar el sistema?",
                "Cerrar Aplicación",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Stop
                );
            if (exit == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            _authenticatedUser = _userLogic.Authenticate(
                txtUser.Text.Trim(),
                txtPassword.Text.Trim()
                );
            
            if (_authenticatedUser.Id is null)
            {
                MessageBox.Show(
                    "El usuario y/o contraseña ingresada no es válido.",
                    "Usuario no encontrado",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                    );
            }
            else if (_authenticatedUser.IsAuthenticated ?? false)
            {
                MessageBox.Show(
                    "El usuario con el que está intentando ingresar, ya cuenta con " +
                    "una sesión activa dentro de nuestro sistema.",
                    "Usuario con sesión activa",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation
                    );
            }
            else if (!_authenticatedUser.IsActive)
            {
                MessageBox.Show(
                   "El usuario con el que está intentando ingresar, no se encuentra " +
                   "activo. Si se trata de un error, contáctese con el administrador " +
                   "del sistema.",
                   "Usuario con sesión activa",
                   MessageBoxButtons.OK,
                   MessageBoxIcon.Exclamation
                   );
            }
            else
            {
                MessageBox.Show(
                   $"Bienvenid@ {_authenticatedUser.FirstName.Split(" ")[0]}!",
                   "Inicio de Sesión satisfactoria",
                   MessageBoxButtons.OK,
                   MessageBoxIcon.Information
                   );

                FrmMain frmMain = new FrmMain(_authenticatedUser);

                // Agrega evento para escuchar el cerrado del formulario principal
                frmMain.FormClosing += FrmMain_FormClosing;

                // TODO: (heavy) Manage permissions
                // then -> Activate controls and update controls with auth user info

                // Mostrar formulario principal
                frmMain.Show();
                // Esconder formulario de inicio de sesión
                this.Hide();
            }
        }

        private void FrmMain_FormClosing(object? sender, FormClosingEventArgs e)
        {
            // Antes de cerrar el formulario
            // Limpiar controles
            ClearTextBoxes();
            // TODO: (light) Cambiar el estado de la sesión en la base de datos

            // Mostrar el formulario de inicio de sesión
            this.Show();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            
        }
    }
}

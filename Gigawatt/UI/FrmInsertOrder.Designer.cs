﻿namespace UI.Logistics.Orders
{
    partial class FrmInsertOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInsertOrder));
            DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
            statusStrip1 = new StatusStrip();
            tsddbEstado = new ToolStripDropDownButton();
            tsmiDone = new ToolStripMenuItem();
            tsmiCanceled = new ToolStripMenuItem();
            tsmiInProgress = new ToolStripMenuItem();
            tsmiPending = new ToolStripMenuItem();
            tsslStatusSelected = new ToolStripStatusLabel();
            toolStripStatusLabel3 = new ToolStripStatusLabel();
            toolStripStatusLabel2 = new ToolStripStatusLabel();
            tsslCurrencySymbol = new ToolStripStatusLabel();
            tsslTotalAmount = new ToolStripStatusLabel();
            toolStripStatusLabel6 = new ToolStripStatusLabel();
            tsslTotalAmountLiteral = new ToolStripStatusLabel();
            toolStripStatusLabel8 = new ToolStripStatusLabel();
            toolStripStatusLabel4 = new ToolStripStatusLabel();
            tsslUsername = new ToolStripStatusLabel();
            menuStrip1 = new MenuStrip();
            toolStripMenuItem1 = new ToolStripMenuItem();
            tabPage1 = new TabPage();
            gbProductInfo = new GroupBox();
            txtValueOrder = new TextBox();
            txtDemand = new TextBox();
            cmbUom = new ComboBox();
            txtProductCode = new TextBox();
            label10 = new Label();
            label12 = new Label();
            btnGetProduct = new Button();
            label13 = new Label();
            txtExternalCode = new TextBox();
            label15 = new Label();
            label14 = new Label();
            label17 = new Label();
            label16 = new Label();
            txtProductName = new TextBox();
            txtUnitPrice = new TextBox();
            btnRemove = new FontAwesome.Sharp.IconButton();
            btnAdd = new FontAwesome.Sharp.IconButton();
            btnEdit = new FontAwesome.Sharp.IconButton();
            btnCancel = new FontAwesome.Sharp.IconButton();
            btnSaveNew = new FontAwesome.Sharp.IconButton();
            btnSaveEdit = new FontAwesome.Sharp.IconButton();
            btnSave = new FontAwesome.Sharp.IconButton();
            groupBox3 = new GroupBox();
            txtProjectName = new TextBox();
            txtRequisitionCode = new TextBox();
            btnGetRequisition = new Button();
            btnGetProject = new Button();
            txtProjectCode = new TextBox();
            label1 = new Label();
            label5 = new Label();
            label6 = new Label();
            groupBox2 = new GroupBox();
            txtVendorName = new TextBox();
            txtVendorCode = new TextBox();
            btnGetVendor = new Button();
            label8 = new Label();
            txtExternalDocument = new TextBox();
            label7 = new Label();
            label11 = new Label();
            groupBox1 = new GroupBox();
            cmbCurrency = new ComboBox();
            txtOrderCode = new TextBox();
            cmbExpenseType = new ComboBox();
            label3 = new Label();
            dtpCreatedAt = new DateTimePicker();
            label2 = new Label();
            label9 = new Label();
            label4 = new Label();
            dgvProducts = new DataGridView();
            dgvcThingObjectCode = new DataGridViewTextBoxColumn();
            dgvcThingObjectDescription = new DataGridViewTextBoxColumn();
            dgvcDemand = new DataGridViewTextBoxColumn();
            dgvcUomAbbreviation = new DataGridViewTextBoxColumn();
            dgvcUnitPrice = new DataGridViewTextBoxColumn();
            dgvcValueOrder = new DataGridViewTextBoxColumn();
            tcOrdenCompra = new TabControl();
            tabPage2 = new TabPage();
            label21 = new Label();
            label20 = new Label();
            label19 = new Label();
            label18 = new Label();
            txtTermsConditions = new TextBox();
            txtObservation = new TextBox();
            tabPage3 = new TabPage();
            groupBox5 = new GroupBox();
            label24 = new Label();
            label22 = new Label();
            textBox1 = new TextBox();
            txtSendMail = new Button();
            groupBox4 = new GroupBox();
            label23 = new Label();
            txtPDFExportLocation = new TextBox();
            comboBox1 = new ComboBox();
            txtBrowsePathForSave = new Button();
            toolTip1 = new ToolTip(components);
            statusStrip1.SuspendLayout();
            menuStrip1.SuspendLayout();
            tabPage1.SuspendLayout();
            gbProductInfo.SuspendLayout();
            groupBox3.SuspendLayout();
            groupBox2.SuspendLayout();
            groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dgvProducts).BeginInit();
            tcOrdenCompra.SuspendLayout();
            tabPage2.SuspendLayout();
            tabPage3.SuspendLayout();
            groupBox5.SuspendLayout();
            groupBox4.SuspendLayout();
            SuspendLayout();
            // 
            // statusStrip1
            // 
            statusStrip1.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            statusStrip1.Items.AddRange(new ToolStripItem[] { tsddbEstado, tsslStatusSelected, toolStripStatusLabel3, toolStripStatusLabel2, tsslCurrencySymbol, tsslTotalAmount, toolStripStatusLabel6, tsslTotalAmountLiteral, toolStripStatusLabel8, toolStripStatusLabel4, tsslUsername });
            statusStrip1.Location = new Point(0, 694);
            statusStrip1.Name = "statusStrip1";
            statusStrip1.Size = new Size(1078, 22);
            statusStrip1.TabIndex = 2;
            statusStrip1.Text = "statusStrip1";
            // 
            // tsddbEstado
            // 
            tsddbEstado.DropDownItems.AddRange(new ToolStripItem[] { tsmiDone, tsmiCanceled, tsmiInProgress, tsmiPending });
            tsddbEstado.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            tsddbEstado.Image = (Image)resources.GetObject("tsddbEstado.Image");
            tsddbEstado.ImageTransparentColor = Color.Magenta;
            tsddbEstado.Name = "tsddbEstado";
            tsddbEstado.Size = new Size(72, 20);
            tsddbEstado.Text = "Estado";
            // 
            // tsmiDone
            // 
            tsmiDone.Image = Properties.Resources.Hopstarter_Soft_Scraps_Button_Blank_Green_16;
            tsmiDone.Name = "tsmiDone";
            tsmiDone.Size = new Size(140, 22);
            tsmiDone.Text = "Finalizado";
            // 
            // tsmiCanceled
            // 
            tsmiCanceled.Image = (Image)resources.GetObject("tsmiCanceled.Image");
            tsmiCanceled.Name = "tsmiCanceled";
            tsmiCanceled.Size = new Size(140, 22);
            tsmiCanceled.Text = "Cancelado";
            // 
            // tsmiInProgress
            // 
            tsmiInProgress.Image = (Image)resources.GetObject("tsmiInProgress.Image");
            tsmiInProgress.Name = "tsmiInProgress";
            tsmiInProgress.Size = new Size(140, 22);
            tsmiInProgress.Text = "En Progreso";
            // 
            // tsmiPending
            // 
            tsmiPending.Image = (Image)resources.GetObject("tsmiPending.Image");
            tsmiPending.Name = "tsmiPending";
            tsmiPending.Size = new Size(140, 22);
            tsmiPending.Text = "Pendiente";
            // 
            // tsslStatusSelected
            // 
            tsslStatusSelected.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            tsslStatusSelected.Name = "tsslStatusSelected";
            tsslStatusSelected.Size = new Size(63, 17);
            tsslStatusSelected.Text = "[status]";
            // 
            // toolStripStatusLabel3
            // 
            toolStripStatusLabel3.ForeColor = SystemColors.ButtonShadow;
            toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            toolStripStatusLabel3.Size = new Size(19, 17);
            toolStripStatusLabel3.Text = "￤";
            // 
            // toolStripStatusLabel2
            // 
            toolStripStatusLabel2.DisplayStyle = ToolStripItemDisplayStyle.Text;
            toolStripStatusLabel2.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            toolStripStatusLabel2.Size = new Size(40, 17);
            toolStripStatusLabel2.Text = "Total :";
            // 
            // tsslCurrencySymbol
            // 
            tsslCurrencySymbol.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            tsslCurrencySymbol.Name = "tsslCurrencySymbol";
            tsslCurrencySymbol.Size = new Size(21, 17);
            tsslCurrencySymbol.Text = "S/";
            // 
            // tsslTotalAmount
            // 
            tsslTotalAmount.DisplayStyle = ToolStripItemDisplayStyle.Text;
            tsslTotalAmount.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            tsslTotalAmount.Name = "tsslTotalAmount";
            tsslTotalAmount.Size = new Size(35, 17);
            tsslTotalAmount.Text = "0.00";
            // 
            // toolStripStatusLabel6
            // 
            toolStripStatusLabel6.ForeColor = SystemColors.ButtonShadow;
            toolStripStatusLabel6.Name = "toolStripStatusLabel6";
            toolStripStatusLabel6.Size = new Size(19, 17);
            toolStripStatusLabel6.Text = "￤";
            // 
            // tsslTotalAmountLiteral
            // 
            tsslTotalAmountLiteral.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            tsslTotalAmountLiteral.Name = "tsslTotalAmountLiteral";
            tsslTotalAmountLiteral.Size = new Size(720, 17);
            tsslTotalAmountLiteral.Spring = true;
            tsslTotalAmountLiteral.Text = "[total_amount_literal]";
            tsslTotalAmountLiteral.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // toolStripStatusLabel8
            // 
            toolStripStatusLabel8.ForeColor = SystemColors.ButtonShadow;
            toolStripStatusLabel8.Name = "toolStripStatusLabel8";
            toolStripStatusLabel8.Size = new Size(19, 17);
            toolStripStatusLabel8.Text = "￤";
            // 
            // toolStripStatusLabel4
            // 
            toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            toolStripStatusLabel4.Size = new Size(0, 17);
            // 
            // tsslUsername
            // 
            tsslUsername.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            tsslUsername.ForeColor = SystemColors.WindowText;
            tsslUsername.Image = (Image)resources.GetObject("tsslUsername.Image");
            tsslUsername.Name = "tsslUsername";
            tsslUsername.Size = new Size(55, 17);
            tsslUsername.Text = "[user]";
            // 
            // menuStrip1
            // 
            menuStrip1.Items.AddRange(new ToolStripItem[] { toolStripMenuItem1 });
            menuStrip1.Location = new Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Size = new Size(1078, 24);
            menuStrip1.TabIndex = 0;
            menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            toolStripMenuItem1.Name = "toolStripMenuItem1";
            toolStripMenuItem1.Size = new Size(60, 20);
            toolStripMenuItem1.Text = "&Archivo";
            // 
            // tabPage1
            // 
            tabPage1.BackColor = SystemColors.Control;
            tabPage1.Controls.Add(gbProductInfo);
            tabPage1.Controls.Add(btnRemove);
            tabPage1.Controls.Add(btnAdd);
            tabPage1.Controls.Add(btnEdit);
            tabPage1.Controls.Add(btnCancel);
            tabPage1.Controls.Add(btnSaveNew);
            tabPage1.Controls.Add(btnSaveEdit);
            tabPage1.Controls.Add(btnSave);
            tabPage1.Controls.Add(groupBox3);
            tabPage1.Controls.Add(groupBox2);
            tabPage1.Controls.Add(groupBox1);
            tabPage1.Controls.Add(dgvProducts);
            tabPage1.Location = new Point(4, 22);
            tabPage1.Margin = new Padding(2, 3, 2, 3);
            tabPage1.Name = "tabPage1";
            tabPage1.Padding = new Padding(2, 3, 2, 3);
            tabPage1.Size = new Size(1070, 638);
            tabPage1.TabIndex = 0;
            tabPage1.Text = "OC";
            // 
            // gbProductInfo
            // 
            gbProductInfo.Controls.Add(txtValueOrder);
            gbProductInfo.Controls.Add(txtDemand);
            gbProductInfo.Controls.Add(cmbUom);
            gbProductInfo.Controls.Add(txtProductCode);
            gbProductInfo.Controls.Add(label10);
            gbProductInfo.Controls.Add(label12);
            gbProductInfo.Controls.Add(btnGetProduct);
            gbProductInfo.Controls.Add(label13);
            gbProductInfo.Controls.Add(txtExternalCode);
            gbProductInfo.Controls.Add(label15);
            gbProductInfo.Controls.Add(label14);
            gbProductInfo.Controls.Add(label17);
            gbProductInfo.Controls.Add(label16);
            gbProductInfo.Controls.Add(txtProductName);
            gbProductInfo.Controls.Add(txtUnitPrice);
            gbProductInfo.Font = new Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point);
            gbProductInfo.Location = new Point(5, 185);
            gbProductInfo.Name = "gbProductInfo";
            gbProductInfo.Size = new Size(1013, 129);
            gbProductInfo.TabIndex = 3;
            gbProductInfo.TabStop = false;
            gbProductInfo.Text = "Información Producto";
            // 
            // txtValueOrder
            // 
            txtValueOrder.Enabled = false;
            txtValueOrder.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtValueOrder.Location = new Point(838, 86);
            txtValueOrder.Name = "txtValueOrder";
            txtValueOrder.PlaceholderText = "#.####";
            txtValueOrder.ReadOnly = true;
            txtValueOrder.Size = new Size(160, 22);
            txtValueOrder.TabIndex = 6;
            txtValueOrder.TextAlign = HorizontalAlignment.Right;
            // 
            // txtDemand
            // 
            txtDemand.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtDemand.Location = new Point(682, 35);
            txtDemand.Name = "txtDemand";
            txtDemand.PlaceholderText = "#.##";
            txtDemand.Size = new Size(141, 22);
            txtDemand.TabIndex = 3;
            txtDemand.TextAlign = HorizontalAlignment.Right;
            txtDemand.TextChanged += txtUnitPrice_TextChanged;
            txtDemand.Validating += txtDemand_Validating;
            // 
            // cmbUom
            // 
            cmbUom.AutoCompleteMode = AutoCompleteMode.Append;
            cmbUom.AutoCompleteSource = AutoCompleteSource.ListItems;
            cmbUom.Font = new Font("Consolas", 8F, FontStyle.Regular, GraphicsUnit.Point);
            cmbUom.FormattingEnabled = true;
            cmbUom.Location = new Point(838, 35);
            cmbUom.MaxDropDownItems = 6;
            cmbUom.Name = "cmbUom";
            cmbUom.Size = new Size(160, 21);
            cmbUom.TabIndex = 5;
            // 
            // txtProductCode
            // 
            txtProductCode.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            txtProductCode.BackColor = SystemColors.Window;
            txtProductCode.BorderStyle = BorderStyle.FixedSingle;
            txtProductCode.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtProductCode.Location = new Point(9, 36);
            txtProductCode.Margin = new Padding(2, 3, 2, 3);
            txtProductCode.Name = "txtProductCode";
            txtProductCode.Size = new Size(131, 22);
            txtProductCode.TabIndex = 0;
            txtProductCode.TextAlign = HorizontalAlignment.Center;
            txtProductCode.TextChanged += txtProductCode_TextChanged;
            txtProductCode.KeyUp += txtProductCode_KeyUp;
            // 
            // label10
            // 
            label10.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            label10.AutoSize = true;
            label10.Font = new Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point);
            label10.Location = new Point(12, 70);
            label10.Margin = new Padding(2, 0, 2, 0);
            label10.Name = "label10";
            label10.Size = new Size(38, 13);
            label10.TabIndex = 6;
            label10.Text = "C. Ext.";
            // 
            // label12
            // 
            label12.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            label12.AutoSize = true;
            label12.Font = new Font("Segoe UI", 8.25F, FontStyle.Bold, GraphicsUnit.Point);
            label12.Location = new Point(9, 19);
            label12.Margin = new Padding(2, 0, 2, 0);
            label12.Name = "label12";
            label12.Size = new Size(64, 13);
            label12.TabIndex = 6;
            label12.Text = "(*) C. Prod.";
            // 
            // btnGetProduct
            // 
            btnGetProduct.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            btnGetProduct.AutoSize = true;
            btnGetProduct.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnGetProduct.Font = new Font("Segoe UI Symbol", 7F, FontStyle.Regular, GraphicsUnit.Point);
            btnGetProduct.Location = new Point(143, 36);
            btnGetProduct.Margin = new Padding(1, 3, 3, 3);
            btnGetProduct.Name = "btnGetProduct";
            btnGetProduct.Size = new Size(27, 22);
            btnGetProduct.TabIndex = 2;
            btnGetProduct.Text = "";
            btnGetProduct.UseVisualStyleBackColor = true;
            btnGetProduct.Click += btnGetProduct_Click;
            // 
            // label13
            // 
            label13.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            label13.AutoSize = true;
            label13.Font = new Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point);
            label13.Location = new Point(188, 19);
            label13.Margin = new Padding(2, 0, 2, 0);
            label13.Name = "label13";
            label13.Size = new Size(47, 13);
            label13.TabIndex = 6;
            label13.Text = "Articulo";
            // 
            // txtExternalCode
            // 
            txtExternalCode.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            txtExternalCode.BorderStyle = BorderStyle.FixedSingle;
            txtExternalCode.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtExternalCode.Location = new Point(11, 87);
            txtExternalCode.Margin = new Padding(2, 3, 2, 3);
            txtExternalCode.Name = "txtExternalCode";
            txtExternalCode.Size = new Size(159, 22);
            txtExternalCode.TabIndex = 1;
            txtExternalCode.TextAlign = HorizontalAlignment.Center;
            // 
            // label15
            // 
            label15.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            label15.AutoSize = true;
            label15.Font = new Font("Segoe UI", 8.25F, FontStyle.Bold, GraphicsUnit.Point);
            label15.Location = new Point(682, 17);
            label15.Margin = new Padding(2, 0, 2, 0);
            label15.Name = "label15";
            label15.Size = new Size(50, 13);
            label15.TabIndex = 6;
            label15.Text = "(*) Cant.";
            // 
            // label14
            // 
            label14.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            label14.AutoSize = true;
            label14.Font = new Font("Segoe UI", 8.25F, FontStyle.Bold, GraphicsUnit.Point);
            label14.Location = new Point(682, 70);
            label14.Margin = new Padding(2, 0, 2, 0);
            label14.Name = "label14";
            label14.Size = new Size(59, 13);
            label14.TabIndex = 6;
            label14.Text = "(*) P. Unit.";
            // 
            // label17
            // 
            label17.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            label17.AutoSize = true;
            label17.Font = new Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point);
            label17.Location = new Point(838, 69);
            label17.Margin = new Padding(2, 0, 2, 0);
            label17.Name = "label17";
            label17.Size = new Size(55, 13);
            label17.TabIndex = 6;
            label17.Text = "Sub Total";
            // 
            // label16
            // 
            label16.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            label16.AutoSize = true;
            label16.Font = new Font("Segoe UI", 8.25F, FontStyle.Bold, GraphicsUnit.Point);
            label16.Location = new Point(845, 17);
            label16.Margin = new Padding(2, 0, 2, 0);
            label16.Name = "label16";
            label16.Size = new Size(49, 13);
            label16.TabIndex = 6;
            label16.Text = "(*) UdM";
            // 
            // txtProductName
            // 
            txtProductName.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            txtProductName.BackColor = SystemColors.ControlLight;
            txtProductName.BorderStyle = BorderStyle.FixedSingle;
            txtProductName.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtProductName.Location = new Point(180, 35);
            txtProductName.Margin = new Padding(2, 3, 2, 3);
            txtProductName.Multiline = true;
            txtProductName.Name = "txtProductName";
            txtProductName.ReadOnly = true;
            txtProductName.ScrollBars = ScrollBars.Vertical;
            txtProductName.Size = new Size(494, 74);
            txtProductName.TabIndex = 2;
            // 
            // txtUnitPrice
            // 
            txtUnitPrice.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            txtUnitPrice.BorderStyle = BorderStyle.FixedSingle;
            txtUnitPrice.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtUnitPrice.Location = new Point(682, 86);
            txtUnitPrice.Margin = new Padding(2, 3, 2, 3);
            txtUnitPrice.Name = "txtUnitPrice";
            txtUnitPrice.PlaceholderText = "#.####";
            txtUnitPrice.Size = new Size(141, 22);
            txtUnitPrice.TabIndex = 4;
            txtUnitPrice.TextAlign = HorizontalAlignment.Right;
            txtUnitPrice.TextChanged += txtUnitPrice_TextChanged;
            txtUnitPrice.Validating += txtUnitPrice_Validating;
            // 
            // btnRemove
            // 
            btnRemove.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            btnRemove.AutoSize = true;
            btnRemove.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnRemove.BackColor = Color.Transparent;
            btnRemove.IconChar = FontAwesome.Sharp.IconChar.TrashAlt;
            btnRemove.IconColor = SystemColors.ControlText;
            btnRemove.IconFont = FontAwesome.Sharp.IconFont.Auto;
            btnRemove.IconSize = 24;
            btnRemove.ImageAlign = ContentAlignment.MiddleLeft;
            btnRemove.Location = new Point(1022, 365);
            btnRemove.Margin = new Padding(2, 3, 2, 3);
            btnRemove.Name = "btnRemove";
            btnRemove.Padding = new Padding(2, 0, 0, 0);
            btnRemove.Size = new Size(32, 30);
            btnRemove.TabIndex = 7;
            btnRemove.TextAlign = ContentAlignment.MiddleRight;
            btnRemove.UseVisualStyleBackColor = false;
            // 
            // btnAdd
            // 
            btnAdd.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            btnAdd.AutoSize = true;
            btnAdd.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnAdd.BackColor = Color.Transparent;
            btnAdd.Font = new Font("Fira Sans", 9F, FontStyle.Regular, GraphicsUnit.Point);
            btnAdd.IconChar = FontAwesome.Sharp.IconChar.ArrowsDownToLine;
            btnAdd.IconColor = SystemColors.ControlText;
            btnAdd.IconFont = FontAwesome.Sharp.IconFont.Auto;
            btnAdd.IconSize = 24;
            btnAdd.Location = new Point(1023, 194);
            btnAdd.Margin = new Padding(2, 3, 2, 3);
            btnAdd.Name = "btnAdd";
            btnAdd.Padding = new Padding(2, 0, 0, 0);
            btnAdd.Size = new Size(32, 30);
            btnAdd.TabIndex = 4;
            btnAdd.TextImageRelation = TextImageRelation.TextAboveImage;
            btnAdd.UseVisualStyleBackColor = false;
            btnAdd.Click += btnAdd_Click;
            // 
            // btnEdit
            // 
            btnEdit.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            btnEdit.AutoSize = true;
            btnEdit.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnEdit.BackColor = Color.Transparent;
            btnEdit.IconChar = FontAwesome.Sharp.IconChar.Edit;
            btnEdit.IconColor = SystemColors.ControlText;
            btnEdit.IconFont = FontAwesome.Sharp.IconFont.Auto;
            btnEdit.IconSize = 24;
            btnEdit.ImageAlign = ContentAlignment.MiddleLeft;
            btnEdit.Location = new Point(1022, 327);
            btnEdit.Margin = new Padding(2, 3, 2, 3);
            btnEdit.Name = "btnEdit";
            btnEdit.Padding = new Padding(2, 0, 0, 0);
            btnEdit.Size = new Size(32, 30);
            btnEdit.TabIndex = 6;
            btnEdit.TextAlign = ContentAlignment.MiddleRight;
            btnEdit.UseVisualStyleBackColor = false;
            btnEdit.Click += btnEdit_Click;
            // 
            // btnCancel
            // 
            btnCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            btnCancel.BackColor = Color.Transparent;
            btnCancel.FlatAppearance.BorderColor = Color.DarkSeaGreen;
            btnCancel.FlatAppearance.BorderSize = 0;
            btnCancel.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btnCancel.IconChar = FontAwesome.Sharp.IconChar.Cancel;
            btnCancel.IconColor = Color.Red;
            btnCancel.IconFont = FontAwesome.Sharp.IconFont.Auto;
            btnCancel.IconSize = 24;
            btnCancel.Location = new Point(7, 574);
            btnCancel.Margin = new Padding(2, 3, 2, 3);
            btnCancel.Name = "btnCancel";
            btnCancel.Padding = new Padding(2, 4, 2, 4);
            btnCancel.Size = new Size(90, 56);
            btnCancel.TabIndex = 9;
            btnCancel.Text = "Cancelar";
            btnCancel.TextImageRelation = TextImageRelation.ImageAboveText;
            btnCancel.UseVisualStyleBackColor = false;
            // 
            // btnSaveNew
            // 
            btnSaveNew.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            btnSaveNew.BackColor = SystemColors.ControlLight;
            btnSaveNew.FlatAppearance.BorderColor = Color.DarkSeaGreen;
            btnSaveNew.FlatAppearance.BorderSize = 0;
            btnSaveNew.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btnSaveNew.IconChar = FontAwesome.Sharp.IconChar.None;
            btnSaveNew.IconColor = Color.Transparent;
            btnSaveNew.IconFont = FontAwesome.Sharp.IconFont.Auto;
            btnSaveNew.IconSize = 24;
            btnSaveNew.Location = new Point(726, 574);
            btnSaveNew.Margin = new Padding(2, 3, 2, 3);
            btnSaveNew.Name = "btnSaveNew";
            btnSaveNew.Padding = new Padding(2, 4, 2, 4);
            btnSaveNew.Size = new Size(90, 56);
            btnSaveNew.TabIndex = 8;
            btnSaveNew.Text = "Guardar y &Nuevo";
            btnSaveNew.UseVisualStyleBackColor = false;
            btnSaveNew.Click += btnSave_Click;
            // 
            // btnSaveEdit
            // 
            btnSaveEdit.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            btnSaveEdit.BackColor = SystemColors.ControlLight;
            btnSaveEdit.FlatAppearance.BorderColor = Color.DarkSeaGreen;
            btnSaveEdit.FlatAppearance.BorderSize = 0;
            btnSaveEdit.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btnSaveEdit.IconChar = FontAwesome.Sharp.IconChar.None;
            btnSaveEdit.IconColor = Color.Transparent;
            btnSaveEdit.IconFont = FontAwesome.Sharp.IconFont.Auto;
            btnSaveEdit.IconSize = 24;
            btnSaveEdit.Location = new Point(827, 574);
            btnSaveEdit.Margin = new Padding(2, 3, 2, 3);
            btnSaveEdit.Name = "btnSaveEdit";
            btnSaveEdit.Padding = new Padding(2, 4, 2, 4);
            btnSaveEdit.Size = new Size(90, 56);
            btnSaveEdit.TabIndex = 8;
            btnSaveEdit.Text = "Guardar y &Editar";
            btnSaveEdit.UseVisualStyleBackColor = false;
            btnSaveEdit.Click += btnSave_Click;
            // 
            // btnSave
            // 
            btnSave.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            btnSave.BackColor = Color.DarkSeaGreen;
            btnSave.FlatAppearance.BorderColor = Color.DarkSeaGreen;
            btnSave.FlatAppearance.BorderSize = 0;
            btnSave.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btnSave.IconChar = FontAwesome.Sharp.IconChar.FloppyDisk;
            btnSave.IconColor = Color.Green;
            btnSave.IconFont = FontAwesome.Sharp.IconFont.Auto;
            btnSave.IconSize = 24;
            btnSave.Location = new Point(928, 574);
            btnSave.Margin = new Padding(2, 3, 2, 3);
            btnSave.Name = "btnSave";
            btnSave.Padding = new Padding(2, 4, 2, 4);
            btnSave.Size = new Size(90, 56);
            btnSave.TabIndex = 8;
            btnSave.Text = "&Guardar";
            btnSave.TextImageRelation = TextImageRelation.ImageAboveText;
            btnSave.UseVisualStyleBackColor = false;
            btnSave.Click += btnSave_Click;
            // 
            // groupBox3
            // 
            groupBox3.Controls.Add(txtProjectName);
            groupBox3.Controls.Add(txtRequisitionCode);
            groupBox3.Controls.Add(btnGetRequisition);
            groupBox3.Controls.Add(btnGetProject);
            groupBox3.Controls.Add(txtProjectCode);
            groupBox3.Controls.Add(label1);
            groupBox3.Controls.Add(label5);
            groupBox3.Controls.Add(label6);
            groupBox3.Font = new Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point);
            groupBox3.Location = new Point(674, 6);
            groupBox3.Name = "groupBox3";
            groupBox3.Size = new Size(380, 171);
            groupBox3.TabIndex = 2;
            groupBox3.TabStop = false;
            groupBox3.Text = "Información Proyecto";
            // 
            // txtProjectName
            // 
            txtProjectName.BackColor = SystemColors.ControlLight;
            txtProjectName.BorderStyle = BorderStyle.FixedSingle;
            txtProjectName.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtProjectName.Location = new Point(13, 80);
            txtProjectName.Margin = new Padding(2, 3, 2, 3);
            txtProjectName.Multiline = true;
            txtProjectName.Name = "txtProjectName";
            txtProjectName.ReadOnly = true;
            txtProjectName.ScrollBars = ScrollBars.Vertical;
            txtProjectName.Size = new Size(355, 75);
            txtProjectName.TabIndex = 4;
            // 
            // txtRequisitionCode
            // 
            txtRequisitionCode.BorderStyle = BorderStyle.FixedSingle;
            txtRequisitionCode.Enabled = false;
            txtRequisitionCode.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtRequisitionCode.Location = new Point(182, 36);
            txtRequisitionCode.Margin = new Padding(2, 3, 2, 3);
            txtRequisitionCode.Name = "txtRequisitionCode";
            txtRequisitionCode.Size = new Size(156, 22);
            txtRequisitionCode.TabIndex = 2;
            txtRequisitionCode.TextAlign = HorizontalAlignment.Center;
            // 
            // btnGetRequisition
            // 
            btnGetRequisition.AutoSize = true;
            btnGetRequisition.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnGetRequisition.Enabled = false;
            btnGetRequisition.Font = new Font("Segoe UI Symbol", 7F, FontStyle.Regular, GraphicsUnit.Point);
            btnGetRequisition.Location = new Point(341, 36);
            btnGetRequisition.Margin = new Padding(1, 3, 3, 3);
            btnGetRequisition.Name = "btnGetRequisition";
            btnGetRequisition.Size = new Size(27, 22);
            btnGetRequisition.TabIndex = 3;
            btnGetRequisition.Text = "";
            btnGetRequisition.UseVisualStyleBackColor = true;
            // 
            // btnGetProject
            // 
            btnGetProject.AutoSize = true;
            btnGetProject.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnGetProject.Font = new Font("Segoe UI Symbol", 7F, FontStyle.Regular, GraphicsUnit.Point);
            btnGetProject.Location = new Point(131, 36);
            btnGetProject.Margin = new Padding(1, 3, 3, 3);
            btnGetProject.Name = "btnGetProject";
            btnGetProject.Size = new Size(27, 22);
            btnGetProject.TabIndex = 1;
            btnGetProject.Text = "";
            btnGetProject.UseVisualStyleBackColor = true;
            btnGetProject.Click += btnGetProject_Click;
            // 
            // txtProjectCode
            // 
            txtProjectCode.BorderStyle = BorderStyle.FixedSingle;
            txtProjectCode.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtProjectCode.Location = new Point(13, 36);
            txtProjectCode.Margin = new Padding(2, 3, 2, 3);
            txtProjectCode.Name = "txtProjectCode";
            txtProjectCode.Size = new Size(115, 22);
            txtProjectCode.TabIndex = 0;
            txtProjectCode.TextAlign = HorizontalAlignment.Center;
            txtProjectCode.KeyUp += txtProjectCode_KeyUp;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point);
            label1.Location = new Point(13, 65);
            label1.Margin = new Padding(2, 0, 2, 0);
            label1.Name = "label1";
            label1.Size = new Size(51, 13);
            label1.TabIndex = 6;
            label1.Text = "Proyecto";
            label1.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Segoe UI", 8.25F, FontStyle.Bold, GraphicsUnit.Point);
            label5.Location = new Point(13, 20);
            label5.Margin = new Padding(2, 0, 2, 0);
            label5.Name = "label5";
            label5.Size = new Size(96, 13);
            label5.TabIndex = 6;
            label5.Text = "(*) Cod. Proyecto";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point);
            label6.Location = new Point(182, 20);
            label6.Margin = new Padding(2, 0, 2, 0);
            label6.Name = "label6";
            label6.Size = new Size(110, 13);
            label6.TabIndex = 6;
            label6.Text = "Cod. Requerimiento";
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(txtVendorName);
            groupBox2.Controls.Add(txtVendorCode);
            groupBox2.Controls.Add(btnGetVendor);
            groupBox2.Controls.Add(label8);
            groupBox2.Controls.Add(txtExternalDocument);
            groupBox2.Controls.Add(label7);
            groupBox2.Controls.Add(label11);
            groupBox2.Font = new Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point);
            groupBox2.Location = new Point(345, 6);
            groupBox2.Name = "groupBox2";
            groupBox2.Size = new Size(323, 171);
            groupBox2.TabIndex = 1;
            groupBox2.TabStop = false;
            groupBox2.Text = "Información Proveedor";
            // 
            // txtVendorName
            // 
            txtVendorName.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            txtVendorName.BackColor = SystemColors.ControlLight;
            txtVendorName.BorderStyle = BorderStyle.FixedSingle;
            txtVendorName.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtVendorName.Location = new Point(12, 80);
            txtVendorName.Margin = new Padding(2, 3, 2, 3);
            txtVendorName.Multiline = true;
            txtVendorName.Name = "txtVendorName";
            txtVendorName.ReadOnly = true;
            txtVendorName.ScrollBars = ScrollBars.Vertical;
            txtVendorName.Size = new Size(299, 75);
            txtVendorName.TabIndex = 3;
            // 
            // txtVendorCode
            // 
            txtVendorCode.BackColor = SystemColors.Window;
            txtVendorCode.BorderStyle = BorderStyle.FixedSingle;
            txtVendorCode.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtVendorCode.Location = new Point(12, 36);
            txtVendorCode.Margin = new Padding(2, 3, 2, 3);
            txtVendorCode.Name = "txtVendorCode";
            txtVendorCode.PlaceholderText = "RUC";
            txtVendorCode.Size = new Size(109, 22);
            txtVendorCode.TabIndex = 0;
            txtVendorCode.TextAlign = HorizontalAlignment.Center;
            txtVendorCode.KeyUp += txtVendorCode_KeyUp;
            txtVendorCode.Leave += txtVendorCode_Leave;
            // 
            // btnGetVendor
            // 
            btnGetVendor.AutoSize = true;
            btnGetVendor.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnGetVendor.Font = new Font("Segoe UI Symbol", 7F, FontStyle.Regular, GraphicsUnit.Point);
            btnGetVendor.Location = new Point(124, 36);
            btnGetVendor.Margin = new Padding(1, 3, 3, 3);
            btnGetVendor.Name = "btnGetVendor";
            btnGetVendor.Size = new Size(27, 22);
            btnGetVendor.TabIndex = 1;
            btnGetVendor.Text = "";
            btnGetVendor.UseVisualStyleBackColor = true;
            btnGetVendor.Click += btnGetVendor_Click;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Font = new Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point);
            label8.Location = new Point(12, 63);
            label8.Margin = new Padding(2, 0, 2, 0);
            label8.Name = "label8";
            label8.Size = new Size(55, 13);
            label8.TabIndex = 6;
            label8.Text = "Rz. Social";
            label8.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // txtExternalDocument
            // 
            txtExternalDocument.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            txtExternalDocument.BorderStyle = BorderStyle.FixedSingle;
            txtExternalDocument.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtExternalDocument.Location = new Point(175, 36);
            txtExternalDocument.Margin = new Padding(2, 3, 2, 3);
            txtExternalDocument.Name = "txtExternalDocument";
            txtExternalDocument.PlaceholderText = "Cotización";
            txtExternalDocument.Size = new Size(136, 22);
            txtExternalDocument.TabIndex = 2;
            txtExternalDocument.TextAlign = HorizontalAlignment.Center;
            // 
            // label7
            // 
            label7.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            label7.AutoSize = true;
            label7.FlatStyle = FlatStyle.Flat;
            label7.Font = new Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point);
            label7.Location = new Point(175, 19);
            label7.Margin = new Padding(2, 0, 2, 0);
            label7.Name = "label7";
            label7.Size = new Size(102, 13);
            label7.TabIndex = 6;
            label7.Text = "Referencia Externa";
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Font = new Font("Segoe UI", 8.25F, FontStyle.Bold, GraphicsUnit.Point);
            label11.Location = new Point(12, 19);
            label11.Margin = new Padding(2, 0, 2, 0);
            label11.Name = "label11";
            label11.Size = new Size(104, 13);
            label11.TabIndex = 6;
            label11.Text = "(*) Cod. Proveedor";
            label11.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(cmbCurrency);
            groupBox1.Controls.Add(txtOrderCode);
            groupBox1.Controls.Add(cmbExpenseType);
            groupBox1.Controls.Add(label3);
            groupBox1.Controls.Add(dtpCreatedAt);
            groupBox1.Controls.Add(label2);
            groupBox1.Controls.Add(label9);
            groupBox1.Controls.Add(label4);
            groupBox1.Font = new Font("Segoe UI", 8F, FontStyle.Regular, GraphicsUnit.Point);
            groupBox1.Location = new Point(5, 6);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(334, 171);
            groupBox1.TabIndex = 0;
            groupBox1.TabStop = false;
            groupBox1.Text = "Información Compra";
            // 
            // cmbCurrency
            // 
            cmbCurrency.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cmbCurrency.AutoCompleteSource = AutoCompleteSource.ListItems;
            cmbCurrency.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            cmbCurrency.FormattingEnabled = true;
            cmbCurrency.Location = new Point(219, 80);
            cmbCurrency.Name = "cmbCurrency";
            cmbCurrency.Size = new Size(107, 22);
            cmbCurrency.TabIndex = 3;
            cmbCurrency.SelectedIndexChanged += cmbCurrency_SelectedIndexChanged;
            // 
            // txtOrderCode
            // 
            txtOrderCode.BorderStyle = BorderStyle.FixedSingle;
            txtOrderCode.Font = new Font("Fira Code Retina", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtOrderCode.Location = new Point(11, 36);
            txtOrderCode.Margin = new Padding(2, 3, 2, 3);
            txtOrderCode.Name = "txtOrderCode";
            txtOrderCode.PlaceholderText = "CP#####-####";
            txtOrderCode.ReadOnly = true;
            txtOrderCode.Size = new Size(198, 22);
            txtOrderCode.TabIndex = 0;
            txtOrderCode.TextAlign = HorizontalAlignment.Center;
            txtOrderCode.KeyUp += txtVendorCode_KeyUp;
            // 
            // cmbExpenseType
            // 
            cmbExpenseType.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cmbExpenseType.AutoCompleteSource = AutoCompleteSource.ListItems;
            cmbExpenseType.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            cmbExpenseType.FormattingEnabled = true;
            cmbExpenseType.Location = new Point(9, 132);
            cmbExpenseType.Name = "cmbExpenseType";
            cmbExpenseType.Size = new Size(317, 22);
            cmbExpenseType.TabIndex = 5;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Segoe UI", 8F, FontStyle.Bold, GraphicsUnit.Point);
            label3.Location = new Point(219, 65);
            label3.Margin = new Padding(2, 0, 2, 0);
            label3.Name = "label3";
            label3.Size = new Size(67, 13);
            label3.TabIndex = 6;
            label3.Text = "(*) Moneda";
            // 
            // dtpCreatedAt
            // 
            dtpCreatedAt.Enabled = false;
            dtpCreatedAt.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dtpCreatedAt.Format = DateTimePickerFormat.Short;
            dtpCreatedAt.Location = new Point(9, 80);
            dtpCreatedAt.Name = "dtpCreatedAt";
            dtpCreatedAt.Size = new Size(200, 22);
            dtpCreatedAt.TabIndex = 2;
            dtpCreatedAt.Value = new DateTime(2023, 2, 17, 12, 34, 9, 0);
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 8F, FontStyle.Regular, GraphicsUnit.Point);
            label2.Location = new Point(11, 19);
            label2.Margin = new Padding(2, 0, 2, 0);
            label2.Name = "label2";
            label2.Size = new Size(67, 13);
            label2.TabIndex = 6;
            label2.Text = "Cod. Orden";
            label2.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new Font("Segoe UI", 8F, FontStyle.Regular, GraphicsUnit.Point);
            label9.Location = new Point(9, 114);
            label9.Margin = new Padding(2, 0, 2, 0);
            label9.Name = "label9";
            label9.Size = new Size(79, 13);
            label9.TabIndex = 6;
            label9.Text = "Tipo de Gasto";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Segoe UI", 8F, FontStyle.Regular, GraphicsUnit.Point);
            label4.Location = new Point(9, 65);
            label4.Margin = new Padding(2, 0, 2, 0);
            label4.Name = "label4";
            label4.Size = new Size(80, 13);
            label4.TabIndex = 5;
            label4.Text = "Fecha Emisión";
            // 
            // dgvProducts
            // 
            dgvProducts.AllowUserToAddRows = false;
            dgvProducts.AllowUserToDeleteRows = false;
            dgvProducts.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = SystemColors.ButtonFace;
            dgvProducts.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dgvProducts.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            dgvProducts.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvProducts.BackgroundColor = SystemColors.Window;
            dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = SystemColors.Control;
            dataGridViewCellStyle2.Font = new Font("Segoe UI", 8F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle2.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = DataGridViewTriState.True;
            dgvProducts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            dgvProducts.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dgvProducts.Columns.AddRange(new DataGridViewColumn[] { dgvcThingObjectCode, dgvcThingObjectDescription, dgvcDemand, dgvcUomAbbreviation, dgvcUnitPrice, dgvcValueOrder });
            dgvProducts.Location = new Point(6, 326);
            dgvProducts.Margin = new Padding(2, 3, 2, 3);
            dgvProducts.MultiSelect = false;
            dgvProducts.Name = "dgvProducts";
            dgvProducts.ReadOnly = true;
            dgvProducts.RowTemplate.Height = 25;
            dgvProducts.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvProducts.Size = new Size(1011, 242);
            dgvProducts.TabIndex = 5;
            dgvProducts.CellFormatting += dgvProducts_CellFormatting;
            // 
            // dgvcThingObjectCode
            // 
            dgvcThingObjectCode.FillWeight = 30F;
            dgvcThingObjectCode.HeaderText = "C. Prod.";
            dgvcThingObjectCode.Name = "dgvcThingObjectCode";
            dgvcThingObjectCode.ReadOnly = true;
            // 
            // dgvcThingObjectDescription
            // 
            dgvcThingObjectDescription.HeaderText = "Producto";
            dgvcThingObjectDescription.Name = "dgvcThingObjectDescription";
            dgvcThingObjectDescription.ReadOnly = true;
            // 
            // dgvcDemand
            // 
            dgvcDemand.FillWeight = 25F;
            dgvcDemand.HeaderText = "Cant.";
            dgvcDemand.Name = "dgvcDemand";
            dgvcDemand.ReadOnly = true;
            // 
            // dgvcUomAbbreviation
            // 
            dgvcUomAbbreviation.FillWeight = 20F;
            dgvcUomAbbreviation.HeaderText = "UdM";
            dgvcUomAbbreviation.Name = "dgvcUomAbbreviation";
            dgvcUomAbbreviation.ReadOnly = true;
            // 
            // dgvcUnitPrice
            // 
            dgvcUnitPrice.FillWeight = 30F;
            dgvcUnitPrice.HeaderText = "P. Unit.";
            dgvcUnitPrice.Name = "dgvcUnitPrice";
            dgvcUnitPrice.ReadOnly = true;
            // 
            // dgvcValueOrder
            // 
            dgvcValueOrder.FillWeight = 30F;
            dgvcValueOrder.HeaderText = "Valor";
            dgvcValueOrder.Name = "dgvcValueOrder";
            dgvcValueOrder.ReadOnly = true;
            // 
            // tcOrdenCompra
            // 
            tcOrdenCompra.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            tcOrdenCompra.Controls.Add(tabPage1);
            tcOrdenCompra.Controls.Add(tabPage2);
            tcOrdenCompra.Controls.Add(tabPage3);
            tcOrdenCompra.Font = new Font("Segoe UI", 8F, FontStyle.Regular, GraphicsUnit.Point);
            tcOrdenCompra.Location = new Point(0, 27);
            tcOrdenCompra.Margin = new Padding(2, 3, 2, 3);
            tcOrdenCompra.Name = "tcOrdenCompra";
            tcOrdenCompra.SelectedIndex = 0;
            tcOrdenCompra.Size = new Size(1078, 664);
            tcOrdenCompra.TabIndex = 1;
            // 
            // tabPage2
            // 
            tabPage2.Controls.Add(label21);
            tabPage2.Controls.Add(label20);
            tabPage2.Controls.Add(label19);
            tabPage2.Controls.Add(label18);
            tabPage2.Controls.Add(txtTermsConditions);
            tabPage2.Controls.Add(txtObservation);
            tabPage2.Location = new Point(4, 22);
            tabPage2.Name = "tabPage2";
            tabPage2.Padding = new Padding(3);
            tabPage2.Size = new Size(1070, 638);
            tabPage2.TabIndex = 1;
            tabPage2.Text = "Términos & Condiciones";
            tabPage2.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            label21.Font = new Font("Segoe UI", 11.25F, FontStyle.Italic, GraphicsUnit.Point);
            label21.ForeColor = SystemColors.GrayText;
            label21.Location = new Point(739, 32);
            label21.Name = "label21";
            label21.Size = new Size(310, 214);
            label21.TabIndex = 2;
            label21.Text = resources.GetString("label21.Text");
            label21.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label20
            // 
            label20.Font = new Font("Segoe UI", 11.25F, FontStyle.Italic, GraphicsUnit.Point);
            label20.ForeColor = SystemColors.GrayText;
            label20.Location = new Point(20, 333);
            label20.Name = "label20";
            label20.Size = new Size(314, 214);
            label20.TabIndex = 2;
            label20.Text = resources.GetString("label20.Text");
            label20.TextAlign = ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            label19.AutoSize = true;
            label19.Location = new Point(921, 316);
            label19.Name = "label19";
            label19.Size = new Size(132, 13);
            label19.TabIndex = 1;
            label19.Text = "Términos && Condiciones";
            // 
            // label18
            // 
            label18.AutoSize = true;
            label18.Location = new Point(20, 15);
            label18.Name = "label18";
            label18.Size = new Size(71, 13);
            label18.TabIndex = 1;
            label18.Text = "Observación";
            // 
            // txtTermsConditions
            // 
            txtTermsConditions.Location = new Point(362, 333);
            txtTermsConditions.Multiline = true;
            txtTermsConditions.Name = "txtTermsConditions";
            txtTermsConditions.Size = new Size(687, 214);
            txtTermsConditions.TabIndex = 0;
            // 
            // txtObservation
            // 
            txtObservation.Location = new Point(20, 32);
            txtObservation.Multiline = true;
            txtObservation.Name = "txtObservation";
            txtObservation.Size = new Size(687, 214);
            txtObservation.TabIndex = 0;
            // 
            // tabPage3
            // 
            tabPage3.Controls.Add(groupBox5);
            tabPage3.Controls.Add(groupBox4);
            tabPage3.Location = new Point(4, 22);
            tabPage3.Name = "tabPage3";
            tabPage3.Padding = new Padding(3);
            tabPage3.Size = new Size(1070, 638);
            tabPage3.TabIndex = 2;
            tabPage3.Text = "Opciones";
            tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            groupBox5.Controls.Add(label24);
            groupBox5.Controls.Add(label22);
            groupBox5.Controls.Add(textBox1);
            groupBox5.Controls.Add(txtSendMail);
            groupBox5.Location = new Point(8, 106);
            groupBox5.Name = "groupBox5";
            groupBox5.Size = new Size(575, 94);
            groupBox5.TabIndex = 4;
            groupBox5.TabStop = false;
            groupBox5.Text = "Enviar por correo:";
            // 
            // label24
            // 
            label24.AutoSize = true;
            label24.Font = new Font("Segoe UI", 9F, FontStyle.Italic, GraphicsUnit.Point);
            label24.ForeColor = SystemColors.GrayText;
            label24.Location = new Point(11, 65);
            label24.Name = "label24";
            label24.Size = new Size(357, 15);
            label24.TabIndex = 3;
            label24.Text = "Si necesita enviar a distintos destinatarios, sepárelos por comas ( , )";
            // 
            // label22
            // 
            label22.AutoSize = true;
            label22.Location = new Point(11, 21);
            label22.Name = "label22";
            label22.Size = new Size(172, 13);
            label22.TabIndex = 3;
            label22.Text = "Dirección de Correo Electrónico:";
            // 
            // textBox1
            // 
            textBox1.Location = new Point(11, 39);
            textBox1.Name = "textBox1";
            textBox1.PlaceholderText = "correo@dominio.dom";
            textBox1.Size = new Size(446, 22);
            textBox1.TabIndex = 2;
            // 
            // txtSendMail
            // 
            txtSendMail.Location = new Point(463, 39);
            txtSendMail.Name = "txtSendMail";
            txtSendMail.Size = new Size(88, 25);
            txtSendMail.TabIndex = 0;
            txtSendMail.Text = "Enviar";
            txtSendMail.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            groupBox4.Controls.Add(label23);
            groupBox4.Controls.Add(txtPDFExportLocation);
            groupBox4.Controls.Add(comboBox1);
            groupBox4.Controls.Add(txtBrowsePathForSave);
            groupBox4.Location = new Point(8, 12);
            groupBox4.Name = "groupBox4";
            groupBox4.Size = new Size(575, 88);
            groupBox4.TabIndex = 4;
            groupBox4.TabStop = false;
            groupBox4.Text = "Guardar en:";
            // 
            // label23
            // 
            label23.AutoSize = true;
            label23.Location = new Point(17, 25);
            label23.Name = "label23";
            label23.Size = new Size(61, 13);
            label23.TabIndex = 3;
            label23.Text = "Ubicación:";
            // 
            // txtPDFExportLocation
            // 
            txtPDFExportLocation.Location = new Point(17, 42);
            txtPDFExportLocation.Name = "txtPDFExportLocation";
            txtPDFExportLocation.PlaceholderText = "//ruta/para/guardar/archivo";
            txtPDFExportLocation.Size = new Size(375, 22);
            txtPDFExportLocation.TabIndex = 2;
            // 
            // comboBox1
            // 
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox1.FormattingEnabled = true;
            comboBox1.Items.AddRange(new object[] { ".xls", ".pdf" });
            comboBox1.Location = new Point(398, 43);
            comboBox1.Name = "comboBox1";
            comboBox1.Size = new Size(59, 21);
            comboBox1.TabIndex = 3;
            // 
            // txtBrowsePathForSave
            // 
            txtBrowsePathForSave.Location = new Point(463, 42);
            txtBrowsePathForSave.Name = "txtBrowsePathForSave";
            txtBrowsePathForSave.Size = new Size(88, 25);
            txtBrowsePathForSave.TabIndex = 0;
            txtBrowsePathForSave.Text = "Examinar...";
            txtBrowsePathForSave.UseVisualStyleBackColor = true;
            txtBrowsePathForSave.Click += txtBrowsePathForSave_Click;
            // 
            // FrmInsertOrder
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            AutoSize = true;
            AutoSizeMode = AutoSizeMode.GrowAndShrink;
            BackColor = SystemColors.Control;
            CancelButton = btnCancel;
            ClientSize = new Size(1078, 716);
            Controls.Add(statusStrip1);
            Controls.Add(menuStrip1);
            Controls.Add(tcOrdenCompra);
            Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            Icon = (Icon)resources.GetObject("$this.Icon");
            Margin = new Padding(2, 3, 2, 3);
            MaximizeBox = false;
            Name = "FrmInsertOrder";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Registrar Orden";
            Load += FrmInsertPurchase_Load;
            statusStrip1.ResumeLayout(false);
            statusStrip1.PerformLayout();
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            tabPage1.ResumeLayout(false);
            tabPage1.PerformLayout();
            gbProductInfo.ResumeLayout(false);
            gbProductInfo.PerformLayout();
            groupBox3.ResumeLayout(false);
            groupBox3.PerformLayout();
            groupBox2.ResumeLayout(false);
            groupBox2.PerformLayout();
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)dgvProducts).EndInit();
            tcOrdenCompra.ResumeLayout(false);
            tabPage2.ResumeLayout(false);
            tabPage2.PerformLayout();
            tabPage3.ResumeLayout(false);
            groupBox5.ResumeLayout(false);
            groupBox5.PerformLayout();
            groupBox4.ResumeLayout(false);
            groupBox4.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private StatusStrip statusStrip1;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem toolStripMenuItem1;
        private ToolStripDropDownButton tsddbEstado;
        private ToolStripStatusLabel toolStripStatusLabel2;
        private ToolStripStatusLabel toolStripStatusLabel3;
        private ToolStripStatusLabel tsslCurrencySymbol;
        private ToolStripStatusLabel tsslTotalAmount;
        private ToolStripStatusLabel toolStripStatusLabel8;
        private ToolStripStatusLabel tsslTotalAmountLiteral;
        private TabPage tabPage1;
        private DataGridView dgvProducts;
        private TabControl tcOrdenCompra;
        private DateTimePicker dtpCreatedAt;
        private ComboBox cmbCurrency;
        private GroupBox groupBox1;
        private TextBox txtOrderCode;
        private Label label3;
        private Label label2;
        private GroupBox groupBox3;
        private GroupBox groupBox2;
        private Label label4;
        private TextBox txtExternalDocument;
        private Label label7;
        private Label label6;
        private Label label5;
        private TextBox txtVendorName;
        private Button btnGetVendor;
        private Label label8;
        private ComboBox cmbExpenseType;
        private Label label9;
        private FontAwesome.Sharp.IconButton btnSave;
        private TextBox txtProductName;
        private Label label16;
        private TextBox txtProductCode;
        private Label label15;
        private TextBox txtExternalCode;
        private Label label13;
        private Label label12;
        private Label label10;
        private FontAwesome.Sharp.IconButton btnRemove;
        private FontAwesome.Sharp.IconButton btnEdit;
        private Button btnGetProduct;
        private FontAwesome.Sharp.IconButton btnCancel;
        private GroupBox gbProductInfo;
        private FontAwesome.Sharp.IconButton btnAdd;
        private TextBox txtProjectName;
        private TextBox txtRequisitionCode;
        private Button btnGetRequisition;
        private Button btnGetProject;
        private TextBox txtProjectCode;
        private Label label1;
        private ToolTip toolTip1;
        private ToolStripStatusLabel toolStripStatusLabel4;
        private ToolStripStatusLabel tsslUsername;
        private ToolStripMenuItem tsmiDone;
        private ToolStripMenuItem tsmiCanceled;
        private ToolStripMenuItem tsmiInProgress;
        private ToolStripStatusLabel toolStripStatusLabel6;
        private ToolStripMenuItem tsmiPending;
        private TextBox txtVendorCode;
        private Label label11;
        private Label label14;
        private Label label17;
        private TextBox txtUnitPrice;
        private ComboBox cmbUom;
        private TextBox txtValueOrder;
        private TextBox txtDemand;
        private TabPage tabPage2;
        private Label label21;
        private Label label20;
        private Label label19;
        private Label label18;
        private TextBox txtTermsConditions;
        private TextBox txtObservation;
        private DataGridViewTextBoxColumn dgvcThingObjectCode;
        private DataGridViewTextBoxColumn dgvcThingObjectDescription;
        private DataGridViewTextBoxColumn dgvcDemand;
        private DataGridViewTextBoxColumn dgvcUomAbbreviation;
        private DataGridViewTextBoxColumn dgvcUnitPrice;
        private DataGridViewTextBoxColumn dgvcValueOrder;
        private TabPage tabPage3;
        private GroupBox groupBox5;
        private Label label24;
        private Label label22;
        private TextBox textBox1;
        private Button txtSendMail;
        private GroupBox groupBox4;
        private Label label23;
        private TextBox txtPDFExportLocation;
        private ComboBox comboBox1;
        private Button txtBrowsePathForSave;
        private FontAwesome.Sharp.IconButton btnSaveEdit;
        private FontAwesome.Sharp.IconButton btnSaveNew;
        private ToolStripStatusLabel tsslStatusSelected;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EntityLayer;
using BusinessLayer;
using UI.Utils;
using System.Reflection;
using System.Windows.Controls;

namespace UI
{
    public partial class ModProduct : Form
    {
        public Thing ThingObject { get; set; }
        private BindingList<Thing> _thingBList { get; set; }
        private readonly ThingLogic _thingLogic = new ThingLogic();

        private ComboObject comboObject = new ComboObject();
        private List<ComboObject> comboObjectList = new List<ComboObject>();

        public ModProduct()
        {
            InitializeComponent();
        }

        private void ModProduct_Load(object sender, EventArgs e)
        {
            _thingBList = new BindingList<Thing>(_thingLogic.GetThings());

            dgvThings.AutoGenerateColumns = false;
            dgvThings.DataSource = _thingBList;
            dgvThings.Columns["dgvcId"].DataPropertyName = "Id";
            dgvThings.Columns["dgvcCode"].DataPropertyName = "Code";
            dgvThings.Columns["dgvcDescription"].DataPropertyName = "Description";

            foreach (DataGridViewColumn dgvColumn in dgvThings.Columns)
            {
                if (dgvColumn.Visible == true)
                {
                    comboObject = new ComboObject
                    {
                        Name = dgvColumn.HeaderText,
                        Value = dgvColumn.DataPropertyName

                    };
                    comboObjectList.Add(comboObject);
                }
            }

            cmbFilter.DataSource = comboObjectList;
            cmbFilter.ValueMember = "Value";
            cmbFilter.DisplayMember = "Name";


            tsslCount.Text = _thingBList.Count.ToString();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            ThingObject = (Thing)dgvThings.CurrentRow.DataBoundItem;
            this.Close();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (txtSearch.Text.Trim() != string.Empty)
            {
                var result = FormUtils.FilterByProperty(
                    _thingBList,
                    cmbFilter.SelectedValue.ToString()!,
                    txtSearch.Text.Trim());

                tsslCount.Text = result.Count.ToString();
                dgvThings.DataSource = result;
            }

            else
            {
                dgvThings.DataSource = _thingBList;
                tsslCount.Text = _thingBList.Count.ToString();
            }
        }

        private void dgvThings_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ThingObject = (Thing)dgvThings.CurrentRow.DataBoundItem;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}

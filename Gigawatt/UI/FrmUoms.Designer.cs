﻿namespace UI
{
    partial class FrmUoms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUoms));
            statusStrip1 = new StatusStrip();
            tsslElementsCount = new ToolStripStatusLabel();
            menuStrip1 = new MenuStrip();
            mantenimientoToolStripMenuItem = new ToolStripMenuItem();
            btnRemove = new FontAwesome.Sharp.IconButton();
            btnEdit = new FontAwesome.Sharp.IconButton();
            btnAdd = new FontAwesome.Sharp.IconButton();
            groupBox1 = new GroupBox();
            cmbBaseUnit = new ComboBox();
            label2 = new Label();
            label5 = new Label();
            label4 = new Label();
            label1 = new Label();
            label9 = new Label();
            txtPluralName = new TextBox();
            txtNumericalValue = new TextBox();
            txtAbbreviation = new TextBox();
            txtName = new TextBox();
            dgvUom = new DataGridView();
            dgvcId = new DataGridViewTextBoxColumn();
            dgvcName = new DataGridViewTextBoxColumn();
            dgvcNamePlural = new DataGridViewTextBoxColumn();
            dgvcAbbreviation = new DataGridViewTextBoxColumn();
            dgvcNumericalValue = new DataGridViewTextBoxColumn();
            BaseUnit = new DataGridViewTextBoxColumn();
            statusStrip1.SuspendLayout();
            menuStrip1.SuspendLayout();
            groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dgvUom).BeginInit();
            SuspendLayout();
            // 
            // statusStrip1
            // 
            statusStrip1.Items.AddRange(new ToolStripItem[] { tsslElementsCount });
            statusStrip1.Location = new Point(0, 417);
            statusStrip1.Name = "statusStrip1";
            statusStrip1.Size = new Size(534, 22);
            statusStrip1.TabIndex = 0;
            statusStrip1.Text = "statusStrip1";
            // 
            // tsslElementsCount
            // 
            tsslElementsCount.Name = "tsslElementsCount";
            tsslElementsCount.Size = new Size(99, 17);
            tsslElementsCount.Text = "[elements_count]";
            // 
            // menuStrip1
            // 
            menuStrip1.Items.AddRange(new ToolStripItem[] { mantenimientoToolStripMenuItem });
            menuStrip1.Location = new Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Size = new Size(534, 24);
            menuStrip1.TabIndex = 1;
            menuStrip1.Text = "menuStrip1";
            // 
            // mantenimientoToolStripMenuItem
            // 
            mantenimientoToolStripMenuItem.Name = "mantenimientoToolStripMenuItem";
            mantenimientoToolStripMenuItem.Size = new Size(60, 20);
            mantenimientoToolStripMenuItem.Text = "&Archivo";
            // 
            // btnRemove
            // 
            btnRemove.AutoSize = true;
            btnRemove.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnRemove.BackColor = Color.Transparent;
            btnRemove.IconChar = FontAwesome.Sharp.IconChar.TrashAlt;
            btnRemove.IconColor = SystemColors.ControlText;
            btnRemove.IconFont = FontAwesome.Sharp.IconFont.Auto;
            btnRemove.IconSize = 24;
            btnRemove.ImageAlign = ContentAlignment.MiddleLeft;
            btnRemove.Location = new Point(48, 152);
            btnRemove.Margin = new Padding(2, 3, 2, 3);
            btnRemove.Name = "btnRemove";
            btnRemove.Padding = new Padding(2, 0, 0, 0);
            btnRemove.Size = new Size(32, 30);
            btnRemove.TabIndex = 3;
            btnRemove.TextAlign = ContentAlignment.MiddleRight;
            btnRemove.UseVisualStyleBackColor = false;
            btnRemove.Click += btnRemove_Click;
            // 
            // btnEdit
            // 
            btnEdit.AutoSize = true;
            btnEdit.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnEdit.BackColor = Color.Transparent;
            btnEdit.IconChar = FontAwesome.Sharp.IconChar.Edit;
            btnEdit.IconColor = SystemColors.ControlText;
            btnEdit.IconFont = FontAwesome.Sharp.IconFont.Auto;
            btnEdit.IconSize = 24;
            btnEdit.ImageAlign = ContentAlignment.MiddleLeft;
            btnEdit.Location = new Point(12, 152);
            btnEdit.Margin = new Padding(2, 3, 2, 3);
            btnEdit.Name = "btnEdit";
            btnEdit.Padding = new Padding(2, 0, 0, 0);
            btnEdit.Size = new Size(32, 30);
            btnEdit.TabIndex = 2;
            btnEdit.TextAlign = ContentAlignment.MiddleRight;
            btnEdit.UseVisualStyleBackColor = false;
            // 
            // btnAdd
            // 
            btnAdd.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            btnAdd.AutoSize = true;
            btnAdd.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnAdd.BackColor = Color.Transparent;
            btnAdd.Font = new Font("Fira Sans", 9F, FontStyle.Regular, GraphicsUnit.Point);
            btnAdd.IconChar = FontAwesome.Sharp.IconChar.ArrowsDownToLine;
            btnAdd.IconColor = SystemColors.ControlText;
            btnAdd.IconFont = FontAwesome.Sharp.IconFont.Auto;
            btnAdd.IconSize = 24;
            btnAdd.Location = new Point(490, 152);
            btnAdd.Margin = new Padding(2, 3, 2, 3);
            btnAdd.Name = "btnAdd";
            btnAdd.Padding = new Padding(2, 0, 0, 0);
            btnAdd.Size = new Size(32, 30);
            btnAdd.TabIndex = 1;
            btnAdd.TextImageRelation = TextImageRelation.TextAboveImage;
            btnAdd.UseVisualStyleBackColor = false;
            btnAdd.Click += btnAdd_Click;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(cmbBaseUnit);
            groupBox1.Controls.Add(label2);
            groupBox1.Controls.Add(label5);
            groupBox1.Controls.Add(label4);
            groupBox1.Controls.Add(label1);
            groupBox1.Controls.Add(label9);
            groupBox1.Controls.Add(txtPluralName);
            groupBox1.Controls.Add(txtNumericalValue);
            groupBox1.Controls.Add(txtAbbreviation);
            groupBox1.Controls.Add(txtName);
            groupBox1.Font = new Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point);
            groupBox1.ForeColor = SystemColors.HotTrack;
            groupBox1.Location = new Point(12, 35);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(510, 111);
            groupBox1.TabIndex = 0;
            groupBox1.TabStop = false;
            groupBox1.Text = "Información UdM";
            // 
            // cmbBaseUnit
            // 
            cmbBaseUnit.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbBaseUnit.FlatStyle = FlatStyle.Flat;
            cmbBaseUnit.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            cmbBaseUnit.FormattingEnabled = true;
            cmbBaseUnit.Location = new Point(258, 76);
            cmbBaseUnit.Name = "cmbBaseUnit";
            cmbBaseUnit.Size = new Size(228, 22);
            cmbBaseUnit.TabIndex = 4;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 8.25F, FontStyle.Bold, GraphicsUnit.Point);
            label2.ForeColor = SystemColors.ControlText;
            label2.Location = new Point(255, 20);
            label2.Margin = new Padding(2, 0, 2, 0);
            label2.Name = "label2";
            label2.Size = new Size(73, 13);
            label2.TabIndex = 9;
            label2.Text = "Abreviatura*";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point);
            label5.ForeColor = SystemColors.ControlText;
            label5.Location = new Point(258, 60);
            label5.Margin = new Padding(2, 0, 2, 0);
            label5.Name = "label5";
            label5.Size = new Size(71, 13);
            label5.TabIndex = 9;
            label5.Text = "Unidad Base";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point);
            label4.ForeColor = SystemColors.ControlText;
            label4.Location = new Point(366, 20);
            label4.Margin = new Padding(2, 0, 2, 0);
            label4.Name = "label4";
            label4.Size = new Size(77, 13);
            label4.TabIndex = 9;
            label4.Text = "Val. Numérico";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point);
            label1.ForeColor = SystemColors.ControlText;
            label1.Location = new Point(10, 60);
            label1.Margin = new Padding(2, 0, 2, 0);
            label1.Name = "label1";
            label1.Size = new Size(80, 13);
            label1.TabIndex = 9;
            label1.Text = "Nombre Plural";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new Font("Segoe UI", 8.25F, FontStyle.Bold, GraphicsUnit.Point);
            label9.ForeColor = SystemColors.ControlText;
            label9.Location = new Point(10, 20);
            label9.Margin = new Padding(2, 0, 2, 0);
            label9.Name = "label9";
            label9.Size = new Size(55, 13);
            label9.TabIndex = 9;
            label9.Text = "Nombre*";
            // 
            // txtPluralName
            // 
            txtPluralName.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            txtPluralName.BorderStyle = BorderStyle.None;
            txtPluralName.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtPluralName.Location = new Point(10, 76);
            txtPluralName.Margin = new Padding(2, 3, 2, 3);
            txtPluralName.Name = "txtPluralName";
            txtPluralName.Size = new Size(232, 15);
            txtPluralName.TabIndex = 3;
            // 
            // txtNumericalValue
            // 
            txtNumericalValue.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            txtNumericalValue.BorderStyle = BorderStyle.None;
            txtNumericalValue.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtNumericalValue.Location = new Point(366, 36);
            txtNumericalValue.Margin = new Padding(2, 3, 2, 3);
            txtNumericalValue.Name = "txtNumericalValue";
            txtNumericalValue.Size = new Size(120, 15);
            txtNumericalValue.TabIndex = 2;
            txtNumericalValue.TextAlign = HorizontalAlignment.Right;
            // 
            // txtAbbreviation
            // 
            txtAbbreviation.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            txtAbbreviation.BorderStyle = BorderStyle.None;
            txtAbbreviation.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtAbbreviation.Location = new Point(255, 36);
            txtAbbreviation.Margin = new Padding(2, 3, 2, 3);
            txtAbbreviation.Name = "txtAbbreviation";
            txtAbbreviation.Size = new Size(90, 15);
            txtAbbreviation.TabIndex = 1;
            txtAbbreviation.TextAlign = HorizontalAlignment.Center;
            // 
            // txtName
            // 
            txtName.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            txtName.BorderStyle = BorderStyle.None;
            txtName.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtName.Location = new Point(10, 36);
            txtName.Margin = new Padding(2, 3, 2, 3);
            txtName.Name = "txtName";
            txtName.Size = new Size(232, 15);
            txtName.TabIndex = 0;
            // 
            // dgvUom
            // 
            dgvUom.AllowUserToAddRows = false;
            dgvUom.AllowUserToDeleteRows = false;
            dgvUom.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = SystemColors.Control;
            dataGridViewCellStyle1.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle1.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = DataGridViewTriState.True;
            dgvUom.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            dgvUom.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvUom.Columns.AddRange(new DataGridViewColumn[] { dgvcId, dgvcName, dgvcNamePlural, dgvcAbbreviation, dgvcNumericalValue, BaseUnit });
            dgvUom.Location = new Point(0, 188);
            dgvUom.MultiSelect = false;
            dgvUom.Name = "dgvUom";
            dgvUom.ReadOnly = true;
            dgvUom.RowTemplate.Height = 25;
            dgvUom.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvUom.Size = new Size(534, 226);
            dgvUom.TabIndex = 4;
            dgvUom.CellDoubleClick += dgvUom_CellDoubleClick;
            dgvUom.CellFormatting += dgvUom_CellFormatting;
            // 
            // dgvcId
            // 
            dgvcId.HeaderText = "Id";
            dgvcId.Name = "dgvcId";
            dgvcId.ReadOnly = true;
            dgvcId.Visible = false;
            // 
            // dgvcName
            // 
            dgvcName.HeaderText = "Nombre";
            dgvcName.Name = "dgvcName";
            dgvcName.ReadOnly = true;
            // 
            // dgvcNamePlural
            // 
            dgvcNamePlural.HeaderText = "Plural";
            dgvcNamePlural.Name = "dgvcNamePlural";
            dgvcNamePlural.ReadOnly = true;
            // 
            // dgvcAbbreviation
            // 
            dgvcAbbreviation.FillWeight = 40F;
            dgvcAbbreviation.HeaderText = "Abr.";
            dgvcAbbreviation.Name = "dgvcAbbreviation";
            dgvcAbbreviation.ReadOnly = true;
            // 
            // dgvcNumericalValue
            // 
            dgvcNumericalValue.FillWeight = 60F;
            dgvcNumericalValue.HeaderText = "Valor";
            dgvcNumericalValue.Name = "dgvcNumericalValue";
            dgvcNumericalValue.ReadOnly = true;
            // 
            // BaseUnit
            // 
            BaseUnit.HeaderText = "Base";
            BaseUnit.Name = "BaseUnit";
            BaseUnit.ReadOnly = true;
            // 
            // FrmUoms
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(534, 439);
            Controls.Add(dgvUom);
            Controls.Add(btnRemove);
            Controls.Add(btnEdit);
            Controls.Add(btnAdd);
            Controls.Add(groupBox1);
            Controls.Add(statusStrip1);
            Controls.Add(menuStrip1);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            Icon = (Icon)resources.GetObject("$this.Icon");
            MainMenuStrip = menuStrip1;
            MaximizeBox = false;
            Name = "FrmUoms";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Unidades de Medida";
            Load += FrmUoms_Load;
            statusStrip1.ResumeLayout(false);
            statusStrip1.PerformLayout();
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)dgvUom).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private StatusStrip statusStrip1;
        private ToolStripStatusLabel tsslElementsCount;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem mantenimientoToolStripMenuItem;
        private FontAwesome.Sharp.IconButton btnRemove;
        private FontAwesome.Sharp.IconButton btnEdit;
        private FontAwesome.Sharp.IconButton btnAdd;
        private GroupBox groupBox1;
        private ComboBox cmbBaseUnit;
        private Label label2;
        private Label label5;
        private Label label4;
        private Label label1;
        private Label label9;
        private TextBox txtPluralName;
        private TextBox txtNumericalValue;
        private TextBox txtAbbreviation;
        private TextBox txtName;
        private DataGridView dgvUom;
        private DataGridViewTextBoxColumn dgvcId;
        private DataGridViewTextBoxColumn dgvcName;
        private DataGridViewTextBoxColumn dgvcNamePlural;
        private DataGridViewTextBoxColumn dgvcAbbreviation;
        private DataGridViewTextBoxColumn dgvcNumericalValue;
        private DataGridViewTextBoxColumn BaseUnit;
    }
}
﻿using BusinessLayer;
using EntityLayer.Universal;
using System.Windows.Forms;
using UI.Utils;

namespace UI
{
    public partial class FrmUoms : Form
    {
        // Private props
        private UnitOfMeasurement _uom = new UnitOfMeasurement();
        private readonly UnitOfMeasurementLogic _uomLogic = new UnitOfMeasurementLogic();
        private bool _editionMode = false;

        // Public props
        private bool EditionMode
        {
            get { return _editionMode; }
            set
            {
                _editionMode = value;

                // Clear controls
                clearControls();
                btnEdit.Enabled = !_editionMode;
                btnRemove.Enabled = !_editionMode;
                dgvUom.Enabled = !_editionMode;
            }
        }

        // Pub Methods
        private void refreshDgv()
        {
            List<UnitOfMeasurement> uomListForDgv = _uomLogic.GetUoms();
            dgvUom.DataSource = null;
            dgvUom.DataSource = uomListForDgv;

            // StatusStrip update
            tsslElementsCount.Text = dgvUom.RowCount.ToString() + " unidad(es)";
        }

        private void clearControls()
        {
            txtName.Text = String.Empty;
            txtPluralName.Text = String.Empty;
            txtAbbreviation.Text = String.Empty;
            txtNumericalValue.Text = String.Empty;
            cmbBaseUnit.SelectedValue = 0;
        }

        public FrmUoms()
        {
            InitializeComponent();
        }

        private void FrmUoms_Load(object sender, EventArgs e)
        {
            // Cargar listas y objetos
            List<UnitOfMeasurement> uomListForDgv = _uomLogic.GetUoms();
            List<UnitOfMeasurement> uomListForCmb = uomListForDgv.ToList();

            // ComboBox cmbBaseUnit
            uomListForCmb.Insert(0, new UnitOfMeasurement { });
            cmbBaseUnit.DataSource = uomListForCmb;
            cmbBaseUnit.ValueMember = "Id";
            cmbBaseUnit.DisplayMember = "Name";
            cmbBaseUnit.SelectedItem = 0;

            // DataGridView dgvUom
            dgvUom.AutoGenerateColumns = false;
            dgvUom.Columns["dgvcId"].DataPropertyName = "Id";
            dgvUom.Columns["dgvcName"].DataPropertyName = "Name";
            dgvUom.Columns["dgvcNamePlural"].DataPropertyName = "NamePlural";
            dgvUom.Columns["dgvcAbbreviation"].DataPropertyName = "Abbreviation";
            dgvUom.Columns["dgvcNumericalValue"].DataPropertyName = "NumericalValue";
            dgvUom.Columns["BaseUnit"].DataPropertyName = "BaseUnit.Name";

            dgvUom.DataSource = uomListForDgv;

            // StatusStrip
            tsslElementsCount.Text = dgvUom.RowCount.ToString() + " unidad(es)";
        }

        private void dgvUom_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (
                (dgvUom.Rows[e.RowIndex].DataBoundItem != null) &&
                (dgvUom.Columns[e.ColumnIndex].DataPropertyName.Contains("."))
                )
            {
                e.Value = FormUtils.BindProperty(
                    dgvUom.Rows[e.RowIndex].DataBoundItem,
                    dgvUom.Columns[e.ColumnIndex].DataPropertyName
                );
            }
        }

        private void dgvUom_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            // Switch EditionMode
            EditionMode = true;

            // Get current row DataBoundItem
            _uom = (UnitOfMeasurement)dgvUom.Rows[e.RowIndex].DataBoundItem;

            // 
            txtName.Text = _uom.Name;
            txtAbbreviation.Text = _uom.Abbreviation;
            txtNumericalValue.Text = _uom.NumericalValue?.ToString();
            txtPluralName.Text = _uom.NamePlural;
            cmbBaseUnit.SelectedValue = _uom.BaseUnit?.Id ?? 0;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (
                txtName.Text.Trim() != String.Empty &&
                txtAbbreviation.Text.Trim() != String.Empty
                )
            {
                if (EditionMode)
                {
                    // Uom instance will be updated with control values
                    _uom.Name = txtName.Text;
                    _uom.NamePlural = txtPluralName.Text;
                    _uom.Abbreviation = txtAbbreviation.Text;
                    _uom.NumericalValue = Convert.ToDouble(txtNumericalValue.Text);
                    _uom.BaseUnitId = (int?)cmbBaseUnit.SelectedValue;

                    // Send Uom instance updated for commit in db
                    _uomLogic.UpdateUom(_uom);

                    // Reload DataGridView
                    refreshDgv();

                    // Select the updated object
                    foreach (DataGridViewRow row in dgvUom.Rows)
                    {
                        DataGridViewCell cell = row.Cells["dgvcId"];
                        if (cell.Value.ToString() == _uom.Id.ToString())
                        {
                            row.Selected = true;
                            dgvUom.CurrentCell = row.Cells["dgvcName"];
                            break;
                        }
                    }

                    // Finally change EditionMode
                    EditionMode = false;
                }
                else
                {
                    // si modo edición no está activado => guardar nuevo y recargar dgv
                    _uom = new UnitOfMeasurement
                    {
                        Name = txtName.Text.Trim(),
                        NamePlural = txtPluralName.Text.Trim(),
                        Abbreviation = txtAbbreviation.Text.Trim(),
                        NumericalValue = Convert.ToDouble(
                            txtNumericalValue.Text.Trim() != "" ?
                            txtNumericalValue.Text.Trim() : "1"),
                        BaseUnitId = (int?)cmbBaseUnit.SelectedValue
                    };
                    _uomLogic.InsertUom(_uom);
                    refreshDgv();
                }
            }

            // Clear Uom object
            _uom = new UnitOfMeasurement();
            // Finally change EditionMode
            EditionMode = false;
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            // Get current Uom selected
            _uom = (UnitOfMeasurement)dgvUom.CurrentRow.DataBoundItem;

            // Send for delete from db
            _uomLogic.RemoveUom(_uom);

            // Clear stuffs
            clearControls();
            _uom = new UnitOfMeasurement();

            // Reload DataGridView
            refreshDgv();
        }
    }
}

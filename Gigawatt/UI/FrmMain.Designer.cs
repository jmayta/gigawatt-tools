﻿namespace UI
{
    partial class FrmMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            menuStrip1 = new MenuStrip();
            sesiónToolStripMenuItem = new ToolStripMenuItem();
            cerrarSesiónToolStripMenuItem = new ToolStripMenuItem();
            logísticaToolStripMenuItem = new ToolStripMenuItem();
            comprasToolStripMenuItem = new ToolStripMenuItem();
            tsmiGenerarOrden = new ToolStripMenuItem();
            mantenimientoToolStripMenuItem = new ToolStripMenuItem();
            logísticaToolStripMenuItem1 = new ToolStripMenuItem();
            comprasToolStripMenuItem1 = new ToolStripMenuItem();
            cargarProveedoresToolStripMenuItem = new ToolStripMenuItem();
            cargarProductosToolStripMenuItem = new ToolStripMenuItem();
            cargarServiciosToolStripMenuItem = new ToolStripMenuItem();
            universalToolStripMenuItem = new ToolStripMenuItem();
            tsmiCurrencies = new ToolStripMenuItem();
            statusStrip1 = new StatusStrip();
            toolStripStatusLabel1 = new ToolStripStatusLabel();
            tsslUsername = new ToolStripStatusLabel();
            menuStrip1.SuspendLayout();
            statusStrip1.SuspendLayout();
            SuspendLayout();
            // 
            // menuStrip1
            // 
            menuStrip1.BackColor = SystemColors.ControlLight;
            menuStrip1.Items.AddRange(new ToolStripItem[] { sesiónToolStripMenuItem, logísticaToolStripMenuItem, mantenimientoToolStripMenuItem });
            menuStrip1.Location = new Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Size = new Size(800, 24);
            menuStrip1.TabIndex = 0;
            menuStrip1.Text = "menuStrip1";
            // 
            // sesiónToolStripMenuItem
            // 
            sesiónToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { cerrarSesiónToolStripMenuItem });
            sesiónToolStripMenuItem.Name = "sesiónToolStripMenuItem";
            sesiónToolStripMenuItem.Size = new Size(53, 20);
            sesiónToolStripMenuItem.Text = "Sesión";
            // 
            // cerrarSesiónToolStripMenuItem
            // 
            cerrarSesiónToolStripMenuItem.Name = "cerrarSesiónToolStripMenuItem";
            cerrarSesiónToolStripMenuItem.Size = new Size(143, 22);
            cerrarSesiónToolStripMenuItem.Text = "Cerrar Sesión";
            // 
            // logísticaToolStripMenuItem
            // 
            logísticaToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { comprasToolStripMenuItem });
            logísticaToolStripMenuItem.Name = "logísticaToolStripMenuItem";
            logísticaToolStripMenuItem.Size = new Size(66, 20);
            logísticaToolStripMenuItem.Text = "Logística";
            // 
            // comprasToolStripMenuItem
            // 
            comprasToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { tsmiGenerarOrden });
            comprasToolStripMenuItem.Name = "comprasToolStripMenuItem";
            comprasToolStripMenuItem.Size = new Size(122, 22);
            comprasToolStripMenuItem.Text = "Compras";
            // 
            // tsmiGenerarOrden
            // 
            tsmiGenerarOrden.Name = "tsmiGenerarOrden";
            tsmiGenerarOrden.Size = new Size(151, 22);
            tsmiGenerarOrden.Text = "Generar Orden";
            tsmiGenerarOrden.Click += tsmiGenerarOrden_Click;
            // 
            // mantenimientoToolStripMenuItem
            // 
            mantenimientoToolStripMenuItem.Alignment = ToolStripItemAlignment.Right;
            mantenimientoToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { logísticaToolStripMenuItem1, universalToolStripMenuItem });
            mantenimientoToolStripMenuItem.Name = "mantenimientoToolStripMenuItem";
            mantenimientoToolStripMenuItem.Size = new Size(101, 20);
            mantenimientoToolStripMenuItem.Text = "Mantenimiento";
            // 
            // logísticaToolStripMenuItem1
            // 
            logísticaToolStripMenuItem1.DropDownItems.AddRange(new ToolStripItem[] { comprasToolStripMenuItem1 });
            logísticaToolStripMenuItem1.Name = "logísticaToolStripMenuItem1";
            logísticaToolStripMenuItem1.Size = new Size(180, 22);
            logísticaToolStripMenuItem1.Text = "Logística";
            // 
            // comprasToolStripMenuItem1
            // 
            comprasToolStripMenuItem1.DropDownItems.AddRange(new ToolStripItem[] { cargarProveedoresToolStripMenuItem, cargarProductosToolStripMenuItem, cargarServiciosToolStripMenuItem });
            comprasToolStripMenuItem1.Name = "comprasToolStripMenuItem1";
            comprasToolStripMenuItem1.Size = new Size(122, 22);
            comprasToolStripMenuItem1.Text = "Compras";
            // 
            // cargarProveedoresToolStripMenuItem
            // 
            cargarProveedoresToolStripMenuItem.Name = "cargarProveedoresToolStripMenuItem";
            cargarProveedoresToolStripMenuItem.Size = new Size(186, 22);
            cargarProveedoresToolStripMenuItem.Text = "Cargar Proveedores...";
            // 
            // cargarProductosToolStripMenuItem
            // 
            cargarProductosToolStripMenuItem.Name = "cargarProductosToolStripMenuItem";
            cargarProductosToolStripMenuItem.Size = new Size(186, 22);
            cargarProductosToolStripMenuItem.Text = "Cargar Artículos...";
            // 
            // cargarServiciosToolStripMenuItem
            // 
            cargarServiciosToolStripMenuItem.Name = "cargarServiciosToolStripMenuItem";
            cargarServiciosToolStripMenuItem.Size = new Size(186, 22);
            cargarServiciosToolStripMenuItem.Text = "Cargar Servicios...";
            // 
            // universalToolStripMenuItem
            // 
            universalToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { tsmiCurrencies });
            universalToolStripMenuItem.Name = "universalToolStripMenuItem";
            universalToolStripMenuItem.Size = new Size(180, 22);
            universalToolStripMenuItem.Text = "Universal";
            // 
            // tsmiCurrencies
            // 
            tsmiCurrencies.Name = "tsmiCurrencies";
            tsmiCurrencies.Size = new Size(180, 22);
            tsmiCurrencies.Text = "Divisas";
            tsmiCurrencies.Click += tsmiCurrencies_Click;
            // 
            // statusStrip1
            // 
            statusStrip1.Items.AddRange(new ToolStripItem[] { toolStripStatusLabel1, tsslUsername });
            statusStrip1.Location = new Point(0, 428);
            statusStrip1.Name = "statusStrip1";
            statusStrip1.Size = new Size(800, 22);
            statusStrip1.TabIndex = 1;
            statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            toolStripStatusLabel1.Size = new Size(692, 17);
            toolStripStatusLabel1.Spring = true;
            // 
            // tsslUsername
            // 
            tsslUsername.Font = new Font("Roboto Mono", 9F, FontStyle.Bold, GraphicsUnit.Point);
            tsslUsername.ForeColor = SystemColors.WindowText;
            tsslUsername.Image = Properties.Resources._285646_lock_icon;
            tsslUsername.ImageAlign = ContentAlignment.MiddleLeft;
            tsslUsername.Name = "tsslUsername";
            tsslUsername.Size = new Size(93, 17);
            tsslUsername.Text = "[username]";
            // 
            // FrmMain
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(statusStrip1);
            Controls.Add(menuStrip1);
            Icon = (Icon)resources.GetObject("$this.Icon");
            MainMenuStrip = menuStrip1;
            Name = "FrmMain";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Gigawatt Tools";
            WindowState = FormWindowState.Maximized;
            Load += FrmMain_Load;
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            statusStrip1.ResumeLayout(false);
            statusStrip1.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private MenuStrip menuStrip1;
        private ToolStripMenuItem logísticaToolStripMenuItem;
        private ToolStripMenuItem comprasToolStripMenuItem;
        private ToolStripMenuItem mantenimientoToolStripMenuItem;
        private ToolStripMenuItem logísticaToolStripMenuItem1;
        private ToolStripMenuItem comprasToolStripMenuItem1;
        private ToolStripMenuItem cargarProveedoresToolStripMenuItem;
        private ToolStripMenuItem cargarProductosToolStripMenuItem;
        private ToolStripMenuItem cargarServiciosToolStripMenuItem;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel tsslUsername;
        private ToolStripMenuItem sesiónToolStripMenuItem;
        private ToolStripMenuItem cerrarSesiónToolStripMenuItem;
        private ToolStripMenuItem universalToolStripMenuItem;
        private ToolStripMenuItem tsmiCurrencies;
        private ToolStripMenuItem tsmiGenerarOrden;
        private ToolStripStatusLabel toolStripStatusLabel1;
    }
}
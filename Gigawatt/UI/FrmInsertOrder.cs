﻿using BusinessLayer;
using EntityLayer;
using EntityLayer.Auth;
using EntityLayer.Universal;
using System.ComponentModel;
using System.Globalization;
using UI.Utils;
using Uom = EntityLayer.Universal.UnitOfMeasurement;
using UomLogic = BusinessLayer.UnitOfMeasurementLogic;

using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Diagnostics;

namespace UI.Logistics.Orders
{
    public partial class FrmInsertOrder : Form
    {
        #region Private Variables and Methods

        // ------------------------------------------------------------------------------
        // Private Variables and Methods
        // ------------------------------------------------------------------------------

        // Currency
        private readonly CurrencyLogic _currencyLogic = new CurrencyLogic();
        private Currency _currency = new Currency();

        // ExpenseType
        private readonly ExpenseTypeLogic _expenseTypeLogic = new ExpenseTypeLogic();

        // Order
        private Order _orderObject = new Order();
        public Order OrderObject { get => _orderObject; set => _orderObject = value; }
        private readonly OrderLogic _orderLogic = new OrderLogic();

        // OrderDetail List
        private BindingList<OrderDetail> _bOrderDetails = new BindingList<OrderDetail>();
        private List<OrderDetail> _orderDetailList = new List<OrderDetail>();
        private OrderDetail _orderDetailObject = new OrderDetail();

        // Partner
        private Partner _vendorObject = new Partner();
        private PartnerLogic _partnerLogic = new PartnerLogic();

        // Project
        private Project _projectObject = new Project();
        private ProjectLogic _projectLogic = new ProjectLogic();

        // Thing
        private Thing _thingObject = new Thing();
        public Thing ThingObject { get => _thingObject; set => _thingObject = value; }
        private readonly ThingLogic _thingLogic = new ThingLogic();

        // User
        private User _user = new User();
        private readonly UserLogic _userLogic = new UserLogic();

        // User Authenticated
        private User? _authenticatedUser { get; set; }

        // Uom
        private readonly UomLogic _uomLogic = new UomLogic();

        // Methods
        // ------------------------------------------------------------------------------

        // Reload DataGridView


        // GetProjectByCode
        private void GetProject()
        {
            if (txtProjectCode.Text.Trim() != string.Empty)
            {
                // Get project info
                _projectObject = _projectLogic
                    .GetProjectByCode(txtProjectCode.Text.Trim(), true);

                if (_projectObject.Id != null)
                {
                    // Fill controls with project info
                    txtProjectName.Text = _projectObject.Name;
                    txtProjectCode.BackColor = Color.Honeydew;
                    txtRequisitionCode.Select();
                }
                else
                {
                    txtProjectCode.BackColor = Color.MistyRose;
                    txtProjectCode.Clear();
                    txtProjectCode.Select();
                    txtProjectName.Clear();
                }
            }
            // If there isn't text in TextBox, open modal
            else
            {
                ModProject modProject = new ModProject(_authenticatedUser!);

                DialogResult dialogResult = modProject.ShowDialog();

                if (dialogResult == DialogResult.OK)
                {
                    _projectObject = modProject.ProjectObject;
                    txtProjectCode.Text = _projectObject.Code;
                    txtProjectName.Text = _projectObject.Name;
                    txtProjectCode.BackColor = Color.Honeydew;
                    txtRequisitionCode.Select();
                }
            }
        }

        // GetThingByCode
        private void _getThing()
        {
            if (txtProductCode.Text.Trim() != string.Empty)
            {
                _thingObject = _thingLogic.GetThingByCode(txtProductCode.Text.Trim());

                if (_thingObject.Id != null)
                {
                    // Fill controls with thing info
                    txtProductName.Text = _thingObject.Description;
                    txtProductCode.BackColor = Color.Honeydew;
                    txtExternalCode.Select();
                }
                else
                {
                    txtProductCode.BackColor = Color.MistyRose;
                    txtProductCode.Clear();
                    txtProductName.Clear();
                    txtProductCode.Select();
                }
            }
            // If there isn't text in TextBox, open modal
            else
            {
                // TODO: (LIGHT) product modal
                ModProduct modProduct = new ModProduct();
                DialogResult dialogResult = modProduct.ShowDialog();
                if (dialogResult == DialogResult.OK && modProduct.ThingObject.Id != null)
                {
                    _thingObject = modProduct.ThingObject;
                    txtProductCode.Text = modProduct.ThingObject.Code;
                    txtProductCode.BackColor = Color.Honeydew;
                    txtProductName.Text = modProduct.ThingObject.Description;
                    txtExternalCode.Select();
                }
            }
        }

        // GetVendorByTaxIdNumber
        private void GetVendor()
        {
            // If there is text in TextBox
            if (txtVendorCode.Text.Trim() != string.Empty)
            {
                // Get vendor info
                _vendorObject = _partnerLogic.GetVendorByTaxIdNumber(txtVendorCode.Text.Trim());
                if (_vendorObject.Id != null)
                {
                    // Fill controls with vendor info
                    txtVendorName.Text = _vendorObject.BusinessName;
                    txtVendorCode.BackColor = Color.Honeydew;
                    txtExternalDocument.Select();
                }
                else
                {
                    txtVendorCode.BackColor = Color.MistyRose;
                    txtVendorCode.Clear();
                    txtVendorCode.Select();
                    txtVendorName.Clear();
                }
            }
            // If there isn't text in TextBox, open modal
            else
            {
                using (ModVendor vendorModal = new ModVendor())
                {
                    DialogResult dialogResult = vendorModal.ShowDialog();
                    if (
                        dialogResult == DialogResult.OK &&
                        vendorModal.VendorObject.Id != null
                        )
                    {
                        _vendorObject = vendorModal.VendorObject;
                        txtVendorCode.Text = _vendorObject.TaxIdNumber;
                        txtVendorCode.BackColor = Color.Honeydew;

                        txtVendorName.Text = _vendorObject.BusinessName;
                        txtExternalDocument.Select();
                    }
                    else
                    {
                        txtVendorName.Select();
                        txtVendorCode.BackColor = Color.MistyRose;
                    }
                }
            }
        }

        // ClearControls
        private void _clearControls(params Control[] controllCollection)
        {
            foreach (Control control in controllCollection)
            {
                if (control is TextBox)
                {
                    ((TextBox)control).Clear();
                }
                else if (control is ComboBox)
                {
                    ((ComboBox)control).SelectedIndex = 0;
                }
            }
        }

        // CheckNoEmpty
        private bool _noEmptyTextBoxesInGroupBox(Control groupBoxName)
        {
            bool noEmptyControls = true;

            foreach (Control control in groupBoxName.Controls)
            {
                if (control is TextBox)
                {
                    if (control.Text.Trim() == String.Empty)
                    {
                        noEmptyControls = false;
                        break;
                    }
                }
            }

            return noEmptyControls;
        }

        private bool _noEmptyTextBoxes(params TextBox[] textBoxCollection)
        {
            bool noEmptyControls = true;

            foreach (TextBox textBox in textBoxCollection)
            {
                if (textBox.Text.Trim() == String.Empty)
                {
                    noEmptyControls = false;
                    break;
                }
            }
            return noEmptyControls;
        }
        #endregion

        // ------------------------------------------------------------------------------
        // Public Variables and Methods
        // ------------------------------------------------------------------------------

        public User? AuthenticatedUser
        {
            // TODO: (LEARN) Investigate about PBKDF2
            get => _authenticatedUser;
        }

        // ------------------------------------------------------------------------------
        // Form Operation
        // ------------------------------------------------------------------------------
        public FrmInsertOrder(User authenticatedUser, Order? orderObj = null)
        {
            InitializeComponent();
            _authenticatedUser = authenticatedUser;
            OrderObject = orderObj ?? new Order();
        }
        public FrmInsertOrder()
        {
            InitializeComponent();
        }

        private void FrmInsertPurchase_Load(object sender, EventArgs e)
        {
            // TODO: (Activar) Post test
            // if (OrderObject.Id == null)
            // {
            //     tcOrdenCompra.TabPages[2].Enabled = false;
            // }
            // Get authenticated user info and show up in ToolStrip
            tsslCurrencySymbol.Text = String.Empty;
            tsslTotalAmount.Text = String.Empty;
            tsslTotalAmountLiteral.Text = String.Empty;
            tsslUsername.Text = AuthenticatedUser?.Username ?? "Anonymous";
            // Set CreatedAt
            dtpCreatedAt.Value = DateTime.Now;

            // Load ComboBox Currency
            List<Currency> currencyList = _currencyLogic.GetCurrencyList();
            cmbCurrency.DataSource = currencyList;
            cmbCurrency.DisplayMember = "Name";
            cmbCurrency.ValueMember = "Id";
            cmbCurrency.SelectedIndex = 0;

            // Load ComboBox ExpenseType
            List<ExpenseType> expenseTypeList = _expenseTypeLogic.GetExpenseTypes();
            cmbExpenseType.DataSource = expenseTypeList;
            cmbExpenseType.DisplayMember = "Description";
            cmbExpenseType.ValueMember = "Id";
            cmbExpenseType.SelectedIndex = cmbExpenseType.FindStringExact("SUMINISTRO DE MATERIALES");

            // Load ComboBox UnitOfMeasurement
            List<Uom> uomList = _uomLogic.GetUoms(false);
            cmbUom.DataSource = uomList;
            cmbUom.ValueMember = "Id";
            cmbUom.DisplayMember = "Name";
            cmbUom.SelectedIndex = 0;

            // DataGridView dgvProducts
            dgvProducts.AutoGenerateColumns = false;
            dgvProducts.DataSource = _bOrderDetails;
            dgvProducts.Columns["dgvcThingObjectCode"].DataPropertyName = "ThingObject.Code";
            dgvProducts.Columns["dgvcThingObjectDescription"].DataPropertyName = "ThingObject.Description";
            dgvProducts.Columns["dgvcDemand"].DataPropertyName = "Demand";
            dgvProducts.Columns["dgvcUomAbbreviation"].DataPropertyName = "UomObject.Abbreviation";
            dgvProducts.Columns["dgvcUnitPrice"].DataPropertyName = "UnitPrice";
            dgvProducts.Columns["dgvcValueOrder"].DataPropertyName = "ValueOrder";
        }

        private void btnGetVendor_Click(object sender, EventArgs e)
        {
            GetVendor();
        }

        private void txtVendorCode_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                GetVendor();
            }
        }

        private void txtProjectCode_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                GetProject();
            }
        }

        private void btnGetProject_Click(object sender, EventArgs e)
        {
            GetProject();
        }

        private void txtProductCode_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                _getThing();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (_noEmptyTextBoxes(
                    txtProductCode,
                    txtProductName,
                    txtDemand,
                    txtUnitPrice
                    ) && cmbUom.SelectedIndex >= 0
                )
            {
                _getThing();
                OrderDetail orderDetail = new OrderDetail();
                orderDetail.Demand = Convert.ToDouble(txtDemand.Text);
                orderDetail.ExternalCode = txtExternalCode.Text.Trim() == string.Empty ?
                    null : txtExternalCode.Text.Trim();
                orderDetail.ThingObject = _thingObject;
                orderDetail.UnitPrice = Convert.ToDecimal(txtUnitPrice.Text);
                orderDetail.UomObject = (Uom)cmbUom.SelectedItem;
                orderDetail.ValueOrder = Convert.ToDecimal(txtValueOrder.Text);

                _bOrderDetails.Add(orderDetail);

                decimal totalAmount = (decimal)_bOrderDetails.Sum(row => row.ValueOrder)!;
                tsslTotalAmount.Text = totalAmount.ToString("F4");
                // Clear and reset controls
                _clearControls(
                    txtProductCode,
                    txtExternalCode,
                    txtProductName,
                    txtDemand,
                    txtUnitPrice,
                    txtValueOrder,
                    cmbUom
                    );
            }
            else
            {
                MessageBox.Show(
                    "Porfavor, rellene todos los campos obligatorios \"(*)\" antes de continuar",
                    "Campos obligatorios vacíos",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Hand
                );
            }
        }

        private void dgvProducts_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if ((dgvProducts.Rows[e.RowIndex].DataBoundItem != null) &&
                (dgvProducts.Columns[e.ColumnIndex].DataPropertyName.Contains('.')))
            {
                e.Value = FormUtils.BindProperty(
                    dgvProducts.Rows[e.RowIndex].DataBoundItem,
                    dgvProducts.Columns[e.ColumnIndex].DataPropertyName
                );
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {

        }

        private void btnGetProduct_Click(object sender, EventArgs e)
        {
            _getThing();
        }

        private void txtProductCode_TextChanged(object sender, EventArgs e)
        {
            _thingObject = new Thing();
            txtProductName.Text = String.Empty;
        }

        private void txtDemand_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtDemand.Text.Trim()))
            {
                if (!double.TryParse(txtDemand.Text.Trim(), out double doubleValue))
                {
                    MessageBox.Show(
                        $"\"{txtDemand.Text.Trim()}\" no es un valor válido.\n" +
                        "Ingrese un valor válido. El campo sólo acepta valores " +
                        "numéricos, enteros o decimales.",
                        "Valor y/o Formato Incorrecto",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error
                    );
                    txtDemand.Select(0, txtDemand.Text.Length);
                    e.Cancel = true;
                }
                txtDemand.Text = doubleValue.ToString("N2");
            }
        }

        private void txtUnitPrice_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtUnitPrice.Text.Trim()))
            {
                if (!double.TryParse(txtUnitPrice.Text.Trim(), out double doubleValue))
                {
                    MessageBox.Show(
                        $"\"{txtUnitPrice.Text.Trim()}\" no es un valor válido.\n" +
                        "Ingrese un valor válido. El campo sólo acepta valores " +
                        "numéricos, enteros o decimales.",
                        "Valor y/o Formato Incorrecto",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error
                    );
                    txtUnitPrice.Select(0, txtUnitPrice.Text.Length);
                    e.Cancel = true;
                }
                txtUnitPrice.Text = doubleValue.ToString("N4");
            }
        }

        private void txtUnitPrice_TextChanged(object sender, EventArgs e)
        {
            if (double.TryParse(txtUnitPrice.Text.Trim(), out double unitPriceValue))
            {
                if (double.TryParse(txtDemand.Text.Trim(), out double demandValue))
                {
                    txtValueOrder.Text = (unitPriceValue * demandValue).ToString("N4");
                }
            }
        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            string uomCode = ((Currency)cmbCurrency.SelectedItem).Code ?? "";
            string uomSymbol = ((Currency)cmbCurrency.SelectedItem).Symbol ?? "";
            tsslCurrencySymbol.Text = $"{uomCode} {uomSymbol}";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            _orderObject = new Order
            {
                ExternalDocument = txtExternalDocument.Text.Trim() == String.Empty ?
                    null : txtExternalDocument.Text.Trim(),
                ExpenseTypeObject = ((ExpenseType)cmbExpenseType.SelectedItem),
                AuthorId = (int)_authenticatedUser!.Id!,
                AuthorObject = _authenticatedUser,
                AmountTotal = decimal.Parse(tsslTotalAmount.Text.Trim(), CultureInfo.InvariantCulture),
                Observation = txtObservation.Text.Trim(),
                RequisitionId = null,
                Status = "pending",
                TermsConditions = txtTermsConditions.Text.Trim(),
                CurrencyObject = (Currency)cmbCurrency.SelectedItem,
                ProjectObject = _projectObject,
                VendorObject = _vendorObject,
                OrderDetails = _bOrderDetails.ToList()
            };

            if (_orderLogic.InsertOrder(_orderObject) != 0)
            {
                MessageBox.Show("Orden guardada con éxito");
                sendMail();
            }
        }

        private void txtBrowsePathForSave_Click(object sender, EventArgs e)
        {
            PreSaveOrderOperations();

            string htmlText = Properties.Resources.order_template.ToString();
            htmlText = htmlText.Replace("@OrderCode", _orderObject.Code);
            htmlText = htmlText.Replace(
                "@OrderDate", _orderObject.CreatedAt.ToString("dd/MM/yyyy") ??
                DateTime.Now.ToString("dd/MM/yyyy")
                );
            // htmlText = htmlText.Replace("@OrderVendorBusinessName", _orderObject.VendorObject.BusinessName);
            // htmlText = htmlText.Replace("@OrderVendorFiscalAddress", _orderObject.VendorObject.FiscalAddress);
            // htmlText = htmlText.Replace("@OrderVendorLocation", _orderObject.VendorObject.Location);
            htmlText = htmlText.Replace("@OrderCurrencyName", _orderObject.CurrencyObject.Name);
            // htmlText = htmlText.Replace("@OrderProjectCode", _orderObject.ProjectObject.Code);
            // htmlText = htmlText.Replace("@OrderRequisitionCode", _orderObject.RequisitionObject?.Id.ToString() ?? "-");
            // htmlText = htmlText.Replace("@OrderAuthorFirstName", _orderObject.AuthorObject.FirstName);
            // htmlText = htmlText.Replace("@OrderAuthorLastName", _orderObject.AuthorObject.LastName);

            string filas = string.Empty;

            foreach (OrderDetail orderDetail in _orderObject.OrderDetails!)
            {
                filas += "<tr>";
                filas += $"<td>{orderDetail.ThingObject.Code}</td>";
                filas += $"<td>{orderDetail.ThingObject.Description}</td>";
                filas += $"<td>{orderDetail.UnitPrice}</td>";
                filas += $"<td>{orderDetail.Demand}</td>";
                filas += $"<td>{orderDetail.UomObject.Abbreviation}</td>";
                filas += $"<td>{orderDetail.ValueOrder}</td>";
                filas += "</tr>";
            }

            htmlText = htmlText.Replace("@FILAS", filas);

            // Save File operation
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.FileName = string.Format("OCGW-{0} {1}_{2}", _orderObject.Code, _orderObject.VendorObject.TaxIdNumber, _orderObject.ProjectObject.Code) + ".pdf";
            saveDialog.Filter = "Pdf Files|*.pdf";
            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                using (FileStream fileStream = new FileStream(saveDialog.FileName, FileMode.Create))
                {
                    Document pdfDocument = new Document(PageSize.A4, 25, 25, 25, 25);
                    PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, fileStream);
                    pdfDocument.Open();

                    using (StringReader stringReader = new StringReader(htmlText))
                    {
                        XMLWorkerHelper.GetInstance().ParseXHtml(pdfWriter, pdfDocument, stringReader);
                    }

                    pdfDocument.Close();
                    fileStream.Close();
                }
            }
        }

        private void PreSaveOrderOperations()
        {
            _orderObject = new Order
            {
                AuthorObject = _authenticatedUser!,
                Code = "000202-2023",
                Observation = "Hello world",
                CurrencyObject = (Currency)cmbCurrency.SelectedItem,
                OrderDetails = _bOrderDetails.ToList(),
                VendorObject = _vendorObject,
                ProjectObject = _projectObject
            };
        }

        private void PostSaveOperations()
        {
            // TODO: (LIGHT) Implement
            // Get created order's code
        }

        private void sendMail()
        {
            var client = new SmtpClient("sandbox.smtp.mailtrap.io", 2525)
            {
                Credentials = new NetworkCredential("5702655bf01dbe", "939ad750ceed11"),
                EnableSsl = true
            };
            client.Send(
                "ayataki.gaming@gmail.com",
                $"{_orderObject.AuthorObject.Email}",
                $"Order created",
                $"Orden de compra creada."
                );
            Debug.WriteLine("Sent");
        }

        private void txtVendorCode_Leave(object sender, EventArgs e)
        {
            var senderHold = sender;
            var eHold = e;
            if (txtVendorCode.Text.Trim() == string.Empty)
            {
                txtVendorCode.BackColor = Color.White;
            }
        }
    }
}

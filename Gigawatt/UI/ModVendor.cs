﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EntityLayer;
using BusinessLayer;
using UI.Utils;


namespace UI
{
    public partial class ModVendor : Form
    {
        // Publics
        public Partner VendorObject { get => _vendorObject; }

        // Privates
        private Partner _vendorObject;
        private readonly PartnerLogic _partnerLogic = new();
        private BindingList<Partner> _partnerBList = new BindingList<Partner>();

        // Methods
        private void ChargeOptionsOnComboBox()
        {
            List<ComboObject> comboObjectList = new List<ComboObject>();
            foreach (DataGridViewColumn dgvColumn in dgvVendors.Columns)
            {
                if (dgvColumn.Visible == true)
                {
                    ComboObject comboObject = new ComboObject()
                    {
                        Name = dgvColumn.HeaderText,
                        Value = dgvColumn.DataPropertyName
                    };
                    comboObjectList.Add(comboObject);
                }
            }
            cmbFilter.ValueMember = "Value";
            cmbFilter.DisplayMember = "Name";
            cmbFilter.DataSource = comboObjectList;
        }

        // Form operation ---------------------------------------------------------------
        // Constructor
        public ModVendor()
        {
            InitializeComponent();
        }

        // Load
        private void ModVendor_Load(object sender, EventArgs e)
        {
            _partnerBList = _partnerLogic.GetVendors();

            // DataGridView Vendors
            dgvVendors.AutoGenerateColumns = false;
            dgvVendors.Columns["dgvcId"].DataPropertyName = "Id";
            dgvVendors.Columns["dgvcTaxIdNumber"].DataPropertyName = "TaxIdNumber";
            dgvVendors.Columns["dgvcBusinessName"].DataPropertyName = "BusinessName";
            dgvVendors.DataSource = _partnerBList;

            // ComboBox Filter
            ChargeOptionsOnComboBox();

            // ToolStrip
            tsslItemsFound.Text = _partnerBList.Count.ToString();
        }

        // Control Events ---------------------------------------------------------------
        private void btnAccept_Click(object sender, EventArgs e)
        {
            _vendorObject = (Partner)dgvVendors.CurrentRow.DataBoundItem;
            this.Close();
        }

        private void dgvVendors_CellDoubleClick(
            object sender, DataGridViewCellEventArgs e)
        {
            _vendorObject = (Partner)dgvVendors.CurrentRow.DataBoundItem;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (txtSearch.Text.Trim() != string.Empty)
            {
                _partnerBList = FormUtils.FilterByProperty(
                    _partnerBList,
                    cmbFilter.SelectedValue.ToString()!,
                    txtSearch.Text.Trim()
                    );
                dgvVendors.DataSource = _partnerBList;
                tsslItemsFound.Text = _partnerBList.Count.ToString();
            }
            else
            {
                _partnerBList = _partnerLogic.GetVendors();
                dgvVendors.DataSource = _partnerBList;
                tsslItemsFound.Text = _partnerBList.Count.ToString();
            }
        }
    }
}
